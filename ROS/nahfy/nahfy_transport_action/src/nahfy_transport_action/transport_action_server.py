#! /usr/bin/env python
import rospy

import actionlib  # contiene la clase SimpleActionServer
from nahfy_transport_action_msg.msg import TransportAction, TransportGoal, TransportResult

class TransportActionServer():

    _result = TransportResult() # messages that are used to publish

    def __init__(self):
        self.action_server = actionlib.SimpleActionServer('transport_action_server', TransportAction, self.cb_execute, False)
        self.action_server.start()


    def cb_execute(self, goal):
        rate = rospy.Rate(1)
        success = True

        print("Transporting object...")

        
        rate.sleep()

        self._result.succes = success
        self.action_server.set_succeeded(self._result)
        




if __name__ == '__main__':
    rospy.init_node("nahfy_transport_action_server")
    server = TransportActionServer()
    rospy.spin()
#! /usr/bin/env python
import rospy

import actionlib  # contiene la clase SimpleActionServer
from nahfy_transport_action_msg.msg import TransportAction, TransportGoal, TransportResult
from nahfy_hw_control_msg.srv import HW_Control, HW_ControlRequest
from nahfy_camera_service_msg.srv import CameraServiceMessage, CameraServiceMessageRequest
from sensor_msgs.msg import Range

import os
import sys

def add_nodes_files():
    path = os.path.normpath(os.path.abspath(__file__))
    path_list = path.split(os.sep)
    nahfy_path = ""
    nahfy_path_petitions = ""
    for folder in path_list:
        if folder != 'nahfy_transport_action':
            nahfy_path = nahfy_path + folder + "/"
            nahfy_path_petitions = nahfy_path_petitions + folder + "/"
        else:
            break
    nahfy_path = nahfy_path + "nahfy_move_action/src/"
    nahfy_path_petitions = nahfy_path_petitions + "nahfy_petitions"
    #print(nahfy_path)
    sys.path.insert(1,nahfy_path)
    sys.path.insert(1,nahfy_path_petitions)
    #print(os.path.dirname(os.path.abspath(file)))


add_nodes_files()

from move_to_coordinate import MoveToCoordinate
from api_petitions import get_patient_room


class TransportActionServer():

    _result = TransportResult() # messages that are used to publish

    def __init__(self):
        self.action_server = actionlib.SimpleActionServer('transport_action_server', TransportAction, self.cb_execute, False)
        self.action_server.start()
        rospy.on_shutdown(self.cancel_go_to)


    def cb_execute(self, goal):
        self.rate = rospy.Rate(1)
        success = True

        print("Transporting object...")

        
        # 1. Move to Storage 
        print(" Transport .... moving to storage")
        x_coordinate = float(goal.origin_x)
        y_coordinate = float(goal.origin_y)
        self.move_to_storage(x_coordinate,y_coordinate) # Retrieve from GOAL the target, type and map

        # 2. Open Box
        rospy.wait_for_service("/hw_control")
        hw_control_service = rospy.ServiceProxy("/hw_control", HW_Control)
        rospy.loginfo("Connecting with hw control service")
        msg_request = HW_ControlRequest()
        msg_request.operation_type = 0 # 0 = Wait to put something

        result = hw_control_service(msg_request)
        rospy.loginfo("Result: %s" % result)
        # Wait until lid close, or timeout
    
        # subscribing to distance
        #self.distance_range = Range()
        #dist_sub = rospy.Subscriber("/nahfy_distance", Range, self.distance_callback)

        # TimeOut
        max_attems = 7
        dist_th = 5.5
        
        #while(max_attems>0 and (self.distance_range.range < dist_th)):
        while(max_attems>0):
            rospy.loginfo("waitting for lid...")
            max_attems = max_attems - 1
            rospy.sleep(1)


        # 3. Move to room
        print(" Transport .... moving to room")
        rospy.sleep(3)
        
        id_patient = goal.id_patient
        self.move_to_room(id_patient) # Retrieve from GOAL the target, type and map


        # 4. Search for patient
        print("Recognising patinent in room...")
        rospy.wait_for_service("/detect_people")
        camera_control_service = rospy.ServiceProxy("/detect_people", CameraServiceMessage)
        rospy.loginfo("Connecting with hw control service")
        msg_request_camera = CameraServiceMessageRequest()
        msg_request_camera.people_number = 7

        result_camera = camera_control_service(msg_request_camera)
        rospy.loginfo("Result: %s" % result_camera)


        # 5. Open Box (for release object)
        msg_request.operation_type = 0 # 0 = Wait to pick object

        result = hw_control_service(msg_request)
        rospy.loginfo("Result: %s" % result)
        # Wait until lid close, or timeout
    
        # TimeOut
        max_attems = 7
        dist_th = 6
        
       # while(max_attems>0 and (self.distance_range.range > dist_th)):
        while(max_attems>0):
            rospy.loginfo("waitting for lid...")
            max_attems = max_attems - 1
            rospy.sleep(1)




        # FINISH!!
        rospy.loginfo("Task succed")

        self._result.succes = success
        self.action_server.set_succeeded(self._result)
        


    def distance_callback(self, msg):
        #print("Distancia, callback")
        self.distance_range = msg


    def move_to_storage(self, x_storage, y_storage):
 
        pos = {'x':x_storage, 'y': y_storage}

        print(pos)
        quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : 0.000, 'r4' : 1.000}

        move_controller = MoveToCoordinate()
        move_controller.go_to(pos, quaternion)

        self.rate.sleep()

    def move_to_room(self, id_target):

        #TODO -- Read from BBDD 
        position_to_move = get_patient_room(id_target)
        pos = {'x':position_to_move[0], 'y': position_to_move[1]}

        print(pos)
        quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : 0.000, 'r4' : 1.000}

        move_controller = MoveToCoordinate()
        move_controller.go_to(pos, quaternion)

        self.rate.sleep()


    def cancel_go_to(self):
        """
        If the goal has been sent, we cancel it
        """

        rospy.loginfo("Stop go to movement")
        rospy.sleep(1)


if __name__ == '__main__':
    rospy.init_node("nahfy_transport_action_server")
    server = TransportActionServer()
    rospy.spin()
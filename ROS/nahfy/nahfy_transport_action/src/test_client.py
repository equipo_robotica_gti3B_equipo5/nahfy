#! /usr/bin/env python

import rospy

import actionlib
from nahfy_transport_action_msg.msg import TransportAction, TransportGoal, TransportResult

rospy.init_node('service_transport_action_client')  # se cra el nodo cliente
client = actionlib.SimpleActionClient('transport_action_server', TransportAction)  # se crea el cliente de la accion
client.wait_for_server()  # esperamos a que el servidor este activo


goal = TransportGoal()  # creamos el goal
goal.origin_x = 0
goal.origin_y = 0
goal.id_patient = 1
goal.number_people_to_search = 1
goal.operation_type = "patient"

#goal.time_to_wait = rospy.Duration.from_sec(5.0)  # completamos el goal con el valor requerido

client.send_goal(goal)  # enviamos el goal
#goal.
rospy.loginfo("Enviado el goal...")
client.wait_for_result()  # esperamos el resultado
#print('Tiempo pasado: %f'%(client.get_result().time_elapsed.to_sec()))  # imprime el resultado

import rospy
import rosunit
import unittest
import rostest
PKG = 'nahfy_transport_action'
NAME = 'nahfy_transport_action_test'

import actionlib  # contiene la clase SimpleActionServer
from nahfy_transport_action_msg.msg import TransportAction, TransportGoal, TransportResult

class TestTransportAction(unittest.TestCase):

    def test_transport_action(self):
       
        rospy.init_node("nahfy_transport_action_test")
        rate = rospy.Rate(0.5)

        # Client waits for server
        client = actionlib.SimpleActionClient('transport_action_server', TransportAction)
        client.wait_for_server()

        # Movement destiny
        goal = TransportGoal()
        client.send_goal(goal) # Sends goal to the server

        client.wait_for_result()
        result = client.get_result()

        self.assertTrue(result)



if __name__ == '__main__':
    rostest.rosrun(PKG, NAME, TestTransportAction)

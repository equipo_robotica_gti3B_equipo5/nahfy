#! /usr/bin/env python

from nahfy_navigation.menu import Menu

import unittest
import rostest



class CaseMenuOptionValues(unittest.TestCase):
    """ Tests the menu options processing, checking the values are the expected ones
    """

    def setUp(self):
        self.__menu = Menu()

    def runTest(self):
      
        self.assertEqual(self.__menu.recognize_option("a"), False)
        self.assertEqual(self.__menu.recognize_option('a'), False)
        self.assertEqual(self.__menu.recognize_option("12"), False)

        self.assertEqual(self.__menu.recognize_option(-1), False)
        self.assertEqual(self.__menu.recognize_option(120), False)


        self.assertEqual(self.__menu.recognize_option(8), True)



class MyTestSuite(unittest.TestSuite):

    def __init__(self):
        super(MyTestSuite, self).__init__()
        self.addTest(CaseMenuOptionValues())

#! /usr/bin/env python

import rospy
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, Point, Quaternion

class MoveToCoordinate():
    """
    MoveToCoordinate is a class created to can move the robot through the map
    """
    def __init__(self):
        """
        Initialize the MoveToCoordinate object, putting a check variable in false to know if a goal is not send.
        Create a simple action client, and wait the server is ready too.
        """
        self.goal_sent = False
        self.client = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        # We wait to the server is active
        self.client.wait_for_server()
        # Overwrite the shutdown to detect the user closing the program or it fail, the goal is cancell
        rospy.on_shutdown(self.cancel_go_to)

    def go_to(self, pos, quat):
        """
        Function to send the robot to a destination position.

        Args:
            pos (dictionary): dictionary of two floats, the position x and the position y on the map
            quat (dictionary): dictionary of four floats, the values of position x, y and z and the rotation

        Returns:
            boolean: returns true if the movement is completed and false if have an error
        """
        self.goal_sent = True
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose = Pose(Point(pos['x'], pos['y'], 0.000),Quaternion(quat['r1'], quat['r2'], quat['r3'], quat['r4']))

        # Start moving
        self.client.send_goal(goal)

        # Wait for the result
        success = self.client.wait_for_result()

        if not success:
            rospy.signal_shutdown("Action server not available")
            rospy.logerr("Action server not available")
            self.client.cancel_goal()
            result = False
        else:
            result_client = self.client.get_result()
            rospy.loginfo(result_client)
            result = True

        self.goal_sent = False
        return result

    def cancel_go_to(self):
        """
        If the goal has been sent, we cancel it
        """
        if self.goal_sent:
            self.client.cancel_goal()
        rospy.loginfo("Stop go to movement")
        rospy.sleep(1)
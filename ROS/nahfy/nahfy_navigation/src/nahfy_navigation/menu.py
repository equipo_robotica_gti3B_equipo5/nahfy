
import rospy
from move_to_coordinate import MoveToCoordinate
from constants import *

class Menu:

    def __init__(self):
        pass

    def initialize_node(self):
        rospy.init_node('main_node')
        rate = rospy.Rate(2)

    def show_menu_on_screen(self):
        """
        Show the possible actions the user can make in the console screen and deppending of the action to send, the program make one action or other
        """
        option = show_options()
        recognize_option(option)


    def show_options(self):
        """
        Display on console screen the options and wait for user input

        Returns:
            int: value of the user selection
        """
        print("---------------------------------------")
        print("-- CHOOSE THE ROOM TO MOVE THE ROBOT --")
        print("---------------------------------------")
        print("0 --> Storage")
        print("1 --> Reception")
        print("2 --> Room 1 (single)")
        print("3 --> Bathroom 1")
        print("4 --> Room 2 (double)")
        print("5 --> Bathroom 2")
        print("6 --> Room 3 (single)")
        print("7 --> Bathroom 3")
        print("8 --> Corridor")
        print("---------------------------------------")
        print("------------ OTHER OPTIONS ------------")
        print("---------------------------------------")
        print("9 --> Go to X and Y position")
        print("10 --> Stop program")
        print("---------------------------------------")
        option = raw_input("Enter the number of the option: ")

        return option

    def get_coordinates_input(self):
        """
        Ask to the user, the coordinate X and the coordinate Y (float values) of a point

        Returns:
            dictionary: dictionary with two keys (x and y) with float values
        """
        x = raw_input("Coordinate X: ")
        y = raw_input("Coordinate Y: ")

        return {'x':x, 'y':y}

    def recognize_option(self, option):
        """
        Depending of the chosen option, the program:
        1. Move the robot to a room
        2. Move to a point written by the user
        3. Stop program execution

        Args:
            option (int): option selected in the input of console by the user
        Returns:
            bool: returns true or false, depends on the option was successfully processed
        """
        success = False
        try:
            option = int(option)
            if option > -1 and option < 11:
                success = True
                if option == 0:
                    move_robot_to_room(STORAGE_POSITION)
                elif option == 1:
                    move_robot_to_room(RECEPTION_POSITION)
                elif option == 2:
                    move_robot_to_room(ROOM_1)
                elif option == 3:
                    move_robot_to_room(BATHROOM_1)
                elif option == 4:
                    move_robot_to_room(ROOM_2)
                elif option == 5:
                    move_robot_to_room(BATHROOM_2)
                elif option == 6:
                    move_robot_to_room(ROOM_3)
                elif option == 7:
                    move_robot_to_room(BATHROOM_3)
                elif option == 8:
                    move_robot_to_room(CORRIDOR_POSITION)
                elif option == 9:
                    point = get_coordinates_input()
                    try:
                        point['x'] = float(point['x'])
                        point['y'] = float(point['y'])
                        move_robot_to_room(point)
                    except ValueError:
                        print("The coordinate X and/or Y isn't number")
                        show_menu_on_screen()
                elif option == 10:
                    rate.sleep()
            else:
                print("The option entered isn't a valid number")
                show_menu_on_screen()

        except ValueError:
            print("The option entered isn't number")
            show_menu_on_screen()
        finally:

            return success


    def move_robot_to_room(self, pos):
        """
        Deppending on a position, the robot is moved to it

        Args:
            pos (dictionary of floats): dictionary with two keys (x and y) with float values
        """
        try:
            navigator = MoveToCoordinate()

            position = pos
            quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : 0.000, 'r4' : 1.000}

            rospy.loginfo("Go to (%s, %s) pose", position['x'], position['y'])
            success = navigator.go_to(position, quaternion)

            if success:
                rospy.loginfo("THE ROBOT ARRIVES TO THE ROOM")
            else:
                rospy.loginfo("THE ROBOT NOT ARRIVE TO THE ROOM. SORRY :(")

            rospy.sleep(1)
            show_menu_on_screen()

        except rospy.ROSInterruptException:
            rospy.loginfo("The program is interrupted")

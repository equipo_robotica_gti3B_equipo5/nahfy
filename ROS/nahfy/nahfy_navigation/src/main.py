#! /usr/bin/env python

import rospy
from move_to_coordinate import MoveToCoordinate
from constants import *

from menu import Menu

program_menu = Menu()

program_menu.initialize_node() # Initialize menu node
program_menu.show_menu_on_screen()


#! /usr/bin/env python

import rospy

import os
import sys

#TODO revisar este codigo
def add_node_file():
    path = os.path.normpath(os.path.abspath(__file__))
    path_list = path.split(os.sep)
    nahfy_path = ""
    for folder in path_list:
        if folder != 'nahfy_diagnose_action':
            nahfy_path = nahfy_path + folder + "/"
        else:
            break
    nahfy_path = nahfy_path + "nahfy_petitions"
    print(nahfy_path)
    sys.path.insert(1,nahfy_path)
    #print(os.path.dirname(os.path.abspath(__file__)))

add_node_file()

from api_petitions import get_patient_room
from api_petitions import post_measures

import actionlib  # contiene la clase SimpleActionServer
from nahfy_diagnose_msg.msg import DiagnoseAction, DiagnoseGoal, DiagnoseResult
from nahfy_band_conection_msg.srv import band_conection, band_conectionRequest

class DiagnoseActionServer():

    _result = DiagnoseResult() # messages that are used to publish

    def __init__(self):
        self.action_server = actionlib.SimpleActionServer('diagnose_action_server', DiagnoseAction, self.cb_execute, False)
        self.action_server.start()
        rospy.on_shutdown(self.cancel_go_to)


    def cb_execute(self, goal):
        rate = rospy.Rate(1)
        success = True

        self.patient = goal.id_patient
        self.type_measure = goal.type

        print("Diagnosing patient")
        
        # TODO 1. Move to patient room
        # TODO 2. Recognise patient
        # TODO 3. Obtain measures
        # TODO 4. Upload measures to database

        # 1. Move to patient room
        print("Diagnose ... Moving to patient room")


        # 2. Recognise patient
        
        # 3. Obtain measures depending on the id measure type
        if self.type_measure == 3:
            rospy.wait_for_service("/band_conection")
            band_conection_service = rospy.ServiceProxy("/band_conection", band_Conection)
            rospy.loginfo("Connecting with band conection service")
            msg_request = band_ConectionRequest()
            msg_request.band_mac = "C1:56:A5:FB:B7:B6"
            msg_request.band_auth_key = "56e698d3bd119d046b0babafdd2de229"

            result = band_conection_service(msg_request)
            rospy.loginfo("Result: %s" % result)

        # 4. Upload measures to database

        # FINISH!!
        rospy.loginfo("Task succed")

        self._result.succes = success
        self.action_server.set_succeeded(self._result)
        
    def cancel_go_to(self):
        '''
        If the goal has been sent, we cancel it
        '''

        rospy.loginfo("Stop go to diagnose")
        rospy.sleep(1)



if __name__ == '__main__':
    rospy.init_node("nahfy_diagnose_action_server")
    server = DiagnoseActionServer()
    rospy.spin()
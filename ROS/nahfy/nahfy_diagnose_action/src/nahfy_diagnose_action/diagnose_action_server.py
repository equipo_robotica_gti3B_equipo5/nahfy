#! /usr/bin/env python
import rospy

import actionlib  # contiene la clase SimpleActionServer
from nahfy_diagnose_msg.msg import DiagnoseAction, DiagnoseGoal, DiagnoseResult

class DiagnoseActionServer():

    _result = DiagnoseResult() # messages that are used to publish

    def __init__(self):
        self.action_server = actionlib.SimpleActionServer('diagnose_action_server', DiagnoseAction, self.cb_execute, False)
        self.action_server.start()


    def cb_execute(self, goal):
        rate = rospy.Rate(1)
        success = True

        print("Diagnosing patient")
        
        rate.sleep()

        self._result.succes = success
        self.action_server.set_succeeded(self._result)
        




if __name__ == '__main__':
    rospy.init_node("nahfy_diagnose_action_server")
    server = DiagnoseActionServer()
    rospy.spin()
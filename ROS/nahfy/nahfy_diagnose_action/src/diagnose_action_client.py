#! /usr/bin/env python
import rospy

import actionlib  # contiene la clase SimpleActionServer
from nahfy_diagnose_msg.msg import DiagnoseAction, DiagnoseGoal, DiagnoseResult


class DiagnoseActionClient():
    

    def __init__(self):
        
        # Client waits for server
        client = actionlib.SimpleActionClient('diagnose_action_server', DiagnoseAction)
        client.wait_for_server()

        # Movement destiny
        goal = DiagnoseGoal()

        # Send goal to server
        client.send_goal(goal) 
        print("ORDER SENT!")

        # Wait for server result
        client.wait_for_result()
        print("Resultado: ",str(client.get_result()))




if __name__ == '__main__':
    rospy.init_node("nahfy_move_action_client")
    
    DiagnoseActionClient()
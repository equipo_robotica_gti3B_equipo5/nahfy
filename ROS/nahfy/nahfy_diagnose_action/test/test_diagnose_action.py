
import rospy
import rosunit
import unittest
import rostest
PKG = 'nahfy_diagnose_action'
NAME = 'nahfy_diagnose_action_test'

import actionlib  # contiene la clase SimpleActionServer
from nahfy_diagnose_msg.msg import DiagnoseAction, DiagnoseGoal, DiagnoseResult

class TestTaskServer(unittest.TestCase):

    def test_diagnose_action_server(self):
       
        rospy.init_node("nahfy_diagnose_action_test")
        rate = rospy.Rate(0.5)

        # Client waits for server
        client = actionlib.SimpleActionClient('diagnose_action_server', DiagnoseAction)
        client.wait_for_server()

        # Movement destiny
        goal = DiagnoseGoal()
        client.send_goal(goal) # Sends goal to the server

        client.wait_for_result()
        result = client.get_result()

        self.assertTrue(result)



if __name__ == '__main__':
    rostest.rosrun(PKG, NAME, TestTaskServer)

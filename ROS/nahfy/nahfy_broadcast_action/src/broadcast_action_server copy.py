#! /usr/bin/env python
import rospy

import actionlib  # contiene la clase SimpleActionServer
from nahfy_broadcast_action_msg.msg import BroadcastAction, BroadcastGoal, BroadcastResult

class BoradcastActionServer():

    _result = BroadcastResult() # messages that are used to publish

    def __init__(self):
        self.action_server = actionlib.SimpleActionServer('broadcast_action_server', BroadcastAction, self.cb_execute, False)
        self.action_server.start()


    def cb_execute(self, goal):
        rate = rospy.Rate(1)
        success = True

        print("Broadcasting...")
        
        self._result.succes = success
        self.action_server.set_succeeded(self._result)

        rate.sleep()
        




if __name__ == '__main__':
    rospy.init_node("nahfy_broadcast_action_server")
    server = BoradcastActionServer()
    rospy.spin()
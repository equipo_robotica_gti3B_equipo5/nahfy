
import rospy
import rosunit
import unittest
import rostest
PKG = 'nahfy_broadcast_action'
NAME = 'nahfy_broadcast_action_test'

import actionlib  # contiene la clase SimpleActionServer
from nahfy_broadcast_action_msg.msg import BroadcastAction, BroadcastGoal, BroadcastResult

class TestBroadcastAction(unittest.TestCase):

    def test_transport_action(self):
       
        rospy.init_node("nahfy_broadcast_action_test")
        rate = rospy.Rate(0.5)

        # Client waits for server
        client = actionlib.SimpleActionClient('broadcast_action_server', BroadcastAction)
        client.wait_for_server()

        # Movement destiny
        goal = BroadcastGoal()
        client.send_goal(goal) # Sends goal to the server

        client.wait_for_result()
        result = client.get_result()

        self.assertTrue(result)



if __name__ == '__main__':
    rostest.rosrun(PKG, NAME, TestBroadcastAction)

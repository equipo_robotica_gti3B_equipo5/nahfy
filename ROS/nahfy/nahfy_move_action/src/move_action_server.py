#! /usr/bin/env python
import rospy

import actionlib  # contiene la clase SimpleActionServer
from nahfy_move_action_msg.msg import MoveAction, MoveGoal, MoveResult
from move_to_coordinate import MoveToCoordinate

class MoveActionServer():
    
    _result = MoveResult() # messages that are used to publish

    def __init__(self):
        self.action_server = actionlib.SimpleActionServer('move_action_server', MoveAction, self.cb_execute, False)
        self.action_server.start()


    def cb_execute(self, goal):
        rate = rospy.Rate(1)
        success = True


        print("EJECUTADO...")

        
        pos = {'x':goal.x_pos, 'y': goal.y_pos}

        print(pos)

        quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : 0.000, 'r4' : 1.000}

        move_controller = MoveToCoordinate()
        move_controller.go_to(pos, quaternion)
        
        rate.sleep()


        self._result.succes = success
        self.action_server.set_succeeded(self._result)
        




if __name__ == '__main__':
    rospy.init_node("nahfy_move_action_server")
    server = MoveActionServer()
    rospy.spin()
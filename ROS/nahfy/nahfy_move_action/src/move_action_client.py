#! /usr/bin/env python
import rospy

import actionlib  # contiene la clase SimpleActionServer
from nahfy_move_action_msg.msg import MoveAction, MoveGoal, MoveResult



class MoveActionClient():
    

    def __init__(self, x_pos, y_pos):
        
        # Client waits for server
        client = actionlib.SimpleActionClient('move_action_server', MoveAction)
        client.wait_for_server()

        # Movement destiny
        goal = MoveGoal()
        goal.x_pos = x_pos
        goal.y_pos = y_pos

        # Send goal to server
        client.send_goal(goal) 
        print("ORDER SENT!")

        # Wait for server result
        client.wait_for_result()
        print("Resultado: ",str(client.get_result()))




if __name__ == '__main__':
    rospy.init_node("nahfy_move_action_client")
    
    x_pos = -5.19
    y_pos = 3.32
    MoveActionClient(x_pos, y_pos)


import rospy
import rosunit
import unittest
import rostest
PKG = 'nahfy_move_action'
NAME = 'nahfy_move_action_test'

import actionlib  # contiene la clase SimpleActionServer
from nahfy_move_action_msg.msg import MoveAction, MoveGoal, MoveResult

class TestTaskServer(unittest.TestCase):

    def test_move_action_server(self):
       
        rospy.init_node("nahfy_move_action_test")
        rate = rospy.Rate(0.5)

        # Client waits for server
        client = actionlib.SimpleActionClient('move_action_server', MoveAction)
        client.wait_for_server()
    
        # Known coordinates
        x_pos = -5.19
        y_pos = 3.32

        # Movement destiny
        goal = MoveGoal()
        goal.x_pos = x_pos
        goal.y_pos = y_pos
        client.send_goal(goal) # Sends goal to the server

        client.wait_for_result()
        result = client.get_result()

        self.assertTrue(result)



if __name__ == '__main__':
    rostest.rosrun(PKG, NAME, TestTaskServer)

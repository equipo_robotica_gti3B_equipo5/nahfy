#! /usr/bin/env python

from nahfy_camera_service_msg.srv import CameraServiceMessage, CameraServiceMessageRequest

import rospy
import rosunit
import unittest
import rostest

PKG = 'nahfy_camera'
NAME = 'nahfy_camera_test'

class TestCameraServer(unittest.TestCase):

    def test_task_server(self):

        rospy.wait_for_service('/detect_people')
        s = rospy.ServiceProxy('/detect_people', CameraServiceMessage)

        number_tosearch_test = 0

        resp = s.call(CameraServiceMessageRequest(number_tosearch_test))

        self.assertEqual(resp.success, True)

if __name__ == '__main__':
    rostest.rosrun(PKG, NAME, TestCameraServer)
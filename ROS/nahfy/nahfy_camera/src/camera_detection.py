import rospy
import cv2
import numpy as np
from nahfy_camera_service_msg.srv import CameraServiceMessage, CameraServiceMessageResponse
from cv_bridge import CvBridge, CvBridgeError
from rospy import service
from sensor_msgs.msg import Image

import boto3
import json

import os
import sys

def add_node_file():
    path = os.path.normpath(os.path.abspath(__file__))
    path_list = path.split(os.sep)
    nahfy_path = ""
    for folder in path_list:
        if folder != 'camera_detection':
            nahfy_path = nahfy_path + folder + "/"
        else:
            break
    nahfy_path = nahfy_path + "nahfy_petitions"
    print(nahfy_path)
    sys.path.insert(1,nahfy_path)

add_node_file()

from api_petitions import get_patients_by_center

class CameraDetection():   

    def __init__(self, idCenter, idPatient):
        self.bridge_object = CvBridge()
        self.bucket='nahfy'
        self.collection_id='people'
        self.client = boto3.client("rekognition","us-east-1")
        self.idCenter = idCenter
        self.idPatient = idPatient
        self.person_detected_number = -1
        self.intent_get_image = 0
        self.image_sub = ""

        self.insert_people_into_collection(self.get_patients_list(self.idCenter))

        rospy.init_node('service_detect_people')
        my_service = rospy.Service('/detect_people',CameraServiceMessage,self.callack_camera_msg)

    def callack_camera_msg(self, request):
        self.image_sub = rospy.Subscriber("/raspicam_node/image",Image,self.camera_callback)
        #self.image_sub = rospy.Subscriber("/turtlebot3/camera/image_raw",Image,self.camera_callback)

        self.intent_get_image = 0
        self.idPatient = request.people_number
        while (request.people_number != self.person_detected_number) and self.intent_get_image < 15:
            rospy.sleep(1)
            print("Not detecting people")
            self.intent_get_image += 1


        response = CameraServiceMessageResponse()
        if self.intent_get_image == 15:
            response.success = False
        else:
            response.success = True

        self.image_sub.unregister()
        return response

    def camera_callback(self,data):

        try:
            # We select bgr8 because is the default OpenCV codification
            cv_image = self.bridge_object.imgmsg_to_cv2(data, desired_encoding="bgr8")
        except CvBridgeError as e:
            print(e)

        self.find_person(cv_image)

    def find_person(self,img):
        self.person_detected_number = -1

        # Convert the image to gray scale
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Load clasificators
        #face_cascade = cv2.CascadeClassifier("/home/laura/catkin_ws/src/imagen_capture/src/clasificadores/haarcascade_fullbody.xml")
        face_cascade = cv2.CascadeClassifier("/home/laura/catkin_ws/src/imagen_capture/src/clasificadores/haarcascade_frontalface_default.xml")

        # Faces detection
        faces = face_cascade.detectMultiScale(img_gray, 1.1, 5)

        # For each one detected, is drawn a rectangle and add +1 to counter
        or (x,y,w,h) in faces:
            # Get only the rectangle of the face (with small margin)
            roi_face = img[y-10:y+h+10, x-10:x+w+10]
            cv2.imwrite("temp_image.jpg",roi_face)
            # Search the image
            self.search_face("temp_image.jpg")
            # Draw the box
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)

        cv2.imshow('Imagen', img)
        key = cv2.waitKey(1)

    def search_face(self, fileName):
        # Put the new imagen to check with the rest
        boto3.client('s3').upload_file(fileName, self.bucket, "temp_image.jpg")
        try:
            response = self.client.search_faces_by_image(CollectionId=self.collection_id,Image={'S3Object':{'Bucket':self.bucket,'Name':fileName}},FaceMatchThreshold=0,MaxFaces=1)
            faceMatches = response['FaceMatches']
            # If the match is correctly, we check the quality of the match and if it is bigger than 80% we conclude the person is correctly identify
            for match in faceMatches:
                if match['Similarity'] >= 80:
                    print("This person is: "+match['Face']['ExternalImageId'])
                    if(match['Face']['ExternalImageId'] == str(self.idPatient)):
                        print("FIND")
                        self.person_detected_number = self.idPatient
        except:
            print("Not find person in aws bucket")

    def add_faces_to_collection(self, filename, facename):
        self.client.index_faces(CollectionId=self.collection_id,Image={'S3Object':{'Bucket':self.bucket,'Name':filename}},ExternalImageId=facename,MaxFaces=1)
        print (filename + ' added')

    def insert_people_into_collection(self, patients):
        # Ponemos las imagenes en la coleccion
        self.client.delete_collection(CollectionId=self.collection_id)
        self.client.create_collection(CollectionId=self.collection_id)

        for name, image in patients.items():
            self.add_faces_to_collection(image, name)

    def get_patients_list(self, idCenter):
        result = get_patients_by_center(idCenter)
        dic = {}
        for patient in result:
            dic[patient['id']] = str(patient['id'])+".jpg"
            
        return dic

if __name__ == '__main__':
    camera = CameraDetection(2,7)
    rospy.loginfo("Service /detect_people is ready")
    rospy.spin()
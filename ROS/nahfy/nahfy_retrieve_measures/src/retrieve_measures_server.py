#! /usr/bin/env python
import rospy

import actionlib # Action Server class inside
from nahfy_retrieve_measures.msg import MeasureAction, MeasureGoal, MeasureResult
#from retrieve_measures import RetrieveMeasures

# Instantiante the measures retriever action server
#rm = RetrieveMeasures()
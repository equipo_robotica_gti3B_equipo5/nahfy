#! /usr/bin/env python
import rospy

import actionlib # Action Server class inside
from nahfy_retrieve_measures.msg import MeasureAction, MeasureGoal, MeasureResult

class RetrieveMeasures():

    def __init__(self, name):
        self.action_name = name

        rospy.init_node("retrieve_measures")
        self.action_server = actionlib.SimpleActionServer('measures',MeasureAction, rm_execute, False)


    def rm_execute(self, goal):
        """ Executes the action depending on the type of measurement 
            retrieved from the input and the patient target
        Args:
            goal (MeasureGoal): contains the measure type (int) and the target (int)
        """
        self.rate = rospy.Rate(1)

        # .... TODO: determine measure type and patient  and proced ... #

        for i in range(0,10):
            rate.sleep()
            print("Measuring....")
        
        print(str(goal.measure_type))
        print(str(goal.target))
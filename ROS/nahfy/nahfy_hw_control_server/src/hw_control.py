#! /usr/bin/env python


import rospy
from std_msgs.msg import Byte

from nahfy_hw_control_msg.srv import HW_Control, HW_ControlResponse


class HWControl():


    def __init__(self):
        self.task_server = rospy.Service('/hw_control', HW_Control, self.task_callback)  # Service creation
        
        rospy.init_node('nahfy_hw_control')  # Node start
        rospy.on_shutdown(self.shutdownhook)
        
        self.rate = rospy.Rate(0.5)


    def task_callback(self, request):
        """ Service callback, adds task to the queue
        """

        message = Byte()
        message.data = 0
        max_attempts = 5

        if(request.operation_type==0): # Open and wait for cargo to be inserted
            
            rospy.loginfo("The service hw_control has been called, waitting...")
            
            pub = rospy.Publisher('servo_open_for_cargo', Byte, queue_size=1)

            # Tries to publish at least one time in topic
            while max_attempts>0:
                max_attempts = max_attempts - 1

                connections = pub.get_num_connections()
                if connections > 0:
                    pub.publish(message)
                    break
                else:
                    rospy.sleep(1)

            rospy.sleep(10) # waits for 10 seconds



            max_attempts = 5

            # closes the lid
            pub = rospy.Publisher('servo_close', Byte, queue_size=1)

            # Tries to publish at least one time in topic
            while max_attempts>0:
                max_attempts = max_attempts - 1

                connections = pub.get_num_connections()
                if connections > 0:
                    pub.publish(message)
                    break
                else:
                    rospy.sleep(1)

                rospy.sleep(2) # waits for 10 seconds

        response = HW_ControlResponse()
        response.success = True

        return response 

    def shutdownhook(self):
        """Stops the server
        """
        print("stoping node task_reader")
        self.ctrl_c = True     



if __name__ == '__main__':

    server = HWControl() # server start

    rospy.loginfo("Service /hw_control is ready")
    rospy.spin()  # holds service open


#!/usr/bin/env python

from nahfy_hw_control_msg.srv import HW_Control, HW_ControlRequest

import rospy
import rosunit
import unittest
import rostest

PKG = 'nahfy_hw_control'
NAME = 'nahfy_hw_control_test'

class TestHWServer(unittest.TestCase):

    def test_task_server(self):

        rospy.wait_for_service('/hw_control')
        s = rospy.ServiceProxy('/hw_control', HW_Control)

        introduce_test = 0
        remove_test = 1

        resp_introduce = s.call(HW_ControlRequest(introduce_test))
        resp_remove = s.call(HW_ControlRequest(remove_test))

        self.assertEqual(resp_introduce.success, True)
        self.assertEqual(resp_remove.success, True)

if __name__ == '__main__':
    rostest.rosrun(PKG, NAME, TestHWServer)
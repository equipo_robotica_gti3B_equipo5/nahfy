import task_manager as tm


# External acion modules
from nahfy_move_action_msg.msg import MoveAction, MoveGoal, MoveResult
from nahfy_diagnose_msg.msg import DiagnoseAction, DiagnoseGoal, DiagnoseResult
from nahfy_transport_action_msg.msg import TransportAction, TransportGoal, TransportResult
from nahfy_broadcast_action_msg.msg import BroadcastAction, BroadcastGoal, BroadcastResult



class RobotTask():
    """ Task to perform by the robot
    """

    def __init__(self, task_type): 
        self.task_type = task_type
        
        self.action = None
        self.action_goal = None # Default goal, empty
        self.action_result = None
        self.action_server_name = None

class RobotTaskMovement(RobotTask):
    """ Movement Task, 
        with input parametes 'x' position and 'y' position
    """

    def __init__(self, x_pos, y_pos):
        RobotTask.__init__(self, tm.TASK_TYPE_NAHFY_MOVE)
        self.x_pos = x_pos
        self.y_pos = y_pos

        self.action = MoveAction
        self.action_goal = MoveGoal() # initialize the correct goal
        self.action_result = MoveResult()
        self.action_server_name = tm.TASK_SERVERS[tm.TASK_TYPE_NAHFY_MOVE] # action server name to push the goal
        
        self.populate_params()

    def populate_params(self):
        """Populates the corresponding goal data
        """

        self.action_goal.x_pos = self.x_pos
        self.action_goal.y_pos = self.y_pos


class RobotTaskDiagnose(RobotTask):
    def __init__(self):
        RobotTask.__init__(self, tm.TASK_TYPE_NAHFY_DIAGNOSE)

        self.action = DiagnoseAction
        self.action_goal = DiagnoseGoal() # initialize the correct goal
        self.action_result = DiagnoseResult()
        self.action_server_name = tm.TASK_SERVERS[tm.TASK_TYPE_NAHFY_DIAGNOSE] # action server name to push the goal


class RobotTaskTransport(RobotTask):
    def __init__(self, operation_type, x_origin, y_origin, id_target, people_to_search):
        RobotTask.__init__(self, tm.TASK_TYPE_NAHFY_TRANSPORT)


        # Patient or location ID and type of target (patient or location)
        self.operation_type = operation_type
        self.x_origin = x_origin
        self.y_origin = y_origin
        self.id_target = id_target
        self.people = people_to_search

        self.action = TransportAction
        self.action_goal = TransportGoal() # initialize the correct goal

        self.action_result = TransportResult()
        self.action_server_name = tm.TASK_SERVERS[tm.TASK_TYPE_NAHFY_TRANSPORT] # action server name to push the goal

        self.populate_params()

    def populate_params(self):
        """Populates the corresponding goal data
        """

        self.action_goal.operation_type = self.operation_type
        self.action_goal.origin_x = self.x_origin
        self.action_goal.origin_y = self.y_origin
        self.action_goal.id_patient = self.id_target
        self.action_goal.number_people_to_search = self.people


class RobotTaskBroadcast(RobotTask):
    def __init__(self):
        RobotTask.__init__(self, tm.TASK_TYPE_NAHFY_BROADCAST)

        self.action = BroadcastAction
        self.action_goal = BroadcastGoal() # initialize the correct goal
        self.action_result = BroadcastResult()
        self.action_server_name = tm.TASK_SERVERS[tm.TASK_TYPE_NAHFY_BROADCAST] # action server name to push the goal

#!/usr/bin/env python

import rospy
import time
from smach import State,StateMachine
import smach_ros

from nahfy_work_task_msg.srv import WorkTask, WorkTaskResponse
from task import *

import actionlib  # contiene la clase SimpleActionServer

from nahfy_move_action_msg.msg import MoveAction, MoveGoal, MoveResult

class Init_FSM(WorkTask):

    def __init__(self, WorkTask):

        self.task = WorkTask # Task sended to be done on the FSM
        self.success = None # Resolution from the Task

        sm = StateMachine(outcomes=['success', 'move_task', 'transport_task', 'giveitem_task', 'chat_task', 'diagnose_task', 'moving_task', 'abort_task'])

        with sm:
            StateMachine.add('Check_Task', Check_Task(self.task, None), transitions={'move_task' : 'Go_Point', 'transport_task' : 'Pick_Item', 'moving_task' : 'Go_To_Patient'}, remapping={'output':'task_data'})
            StateMachine.add('Pick_Item', Pick_Item(self.task), transitions={'giveitem_task' : 'Go_To_Patient', 'abort_task' : 'abort_task'}, remapping={'input':'task_data','output':'task_result'})
            StateMachine.add('Go_To_Patient', Go_To_Patient(self.task), transitions={'chat_task' : 'Activate_Chat', 'giveitem_task' : 'Give_Item', 'diagnose_task' : 'Take_Measures', 'abort_task' : 'abort_task'}, remapping={'input':'task_data','output':'task_result'})
            StateMachine.add('Take_Measures', Take_Measures(self.task), transitions={'success' : 'success', 'abort_task' : 'abort_task'}, remapping={'input':'task_data','output':'task_result'})
            StateMachine.add('Activate_Chat', Activate_Chat(self.task), transitions={'success' : 'success', 'abort_task' : 'abort_task'}, remapping={'input':'task_data','output':'task_result'})
            StateMachine.add('Give_Item', Give_Item(self.task), transitions={'success' : 'success', 'abort_task' : 'abort_task'}, remapping={'input':'task_data','output':'task_result'})
            StateMachine.add('Go_Point', Go_Point(self.task), transitions={'success' : 'success', 'abort_task' : 'abort_task'}, remapping={'input':'task_data','output':'task_result'})
        
        sis = smach_ros.IntrospectionServer('nahfy_fsm_server', sm, '/SM_ROOT')
        sis.start()

        self.success = sm.execute() # Saving the result inside the object


class Check_Task(State):
    """
    This class represents the state Check_Task, and this State will use a Task object to control the action / state the robot has to do

    Args:
        State (): Represents a moment of our robot in execution
    """
    def __init__(self, task_to_do, task_resolution):
        State.__init__(self, outcomes=['move_task', 'transport_task', 'moving_task', 'abort_task'], output_keys=['task_result'])
        self.task_to_do = task_to_do
        self.task_resolution = task_resolution

    def execute(self, userdata):
        print("Leyendo tareas asignadas al robot...")
        if self.task_to_do.task_type == "NAHFY_MOVE":
            userdata.task_result = self.task_resolution
            return 'move_task'
        elif self.task_to_do.task_type == "NAHFY_DIAGNOSE" or self.task_to_do.task_type == "NAHFY_BROADCAST":
            userdata.task_result = self.task_resolution
            return 'moving_task'
        elif self.task_to_do.task_type == "NAHFY_TRANSPORT":
            userdata.task_result = self.task_resolution
            return 'transport_task'

class Pick_Item(State):
    """
    This class represents the state Pick_Item, and this State will make the robot to take a object given as a input_key on the storage room

    Args:
        State (): Represents a moment of our robot in execution
    """
    def __init__(self, task_to_do):
        State.__init__(self, outcomes=['giveitem_task', 'abort_task'], input_keys=['task_data'], output_keys=['task_result'])
        self.task_to_do = task_to_do

    def execute(self, userdata):
        print("Cogiendo el item del almacen...")
        # TODO : This State will be implemented for executing code for picking items from storage. We suppose it works correctly
        cont = 0
        while cont<6:
            print(".")
            cont +=1
        if cont == 6:
            userdata.task_result = userdata.task_data
            return 'giveitem_task'
        else:
            success = 'abort_task'
            userdata.task_result = success
            return 'abort_task'

class Go_To_Patient(State):
    """
    This class represents the state Go_To_Patient, and this State will differenciate between the different tasks that have to be done to a pacient

    Args:
        State (): Represents a moment of our robot in execution
    """
    def __init__(self, task_to_do):
        State.__init__(self, outcomes=['chat_task', 'giveitem_task', 'diagnose_task', 'abort_task'], input_keys=['task_data'], output_keys=['task_result'])
        self.task_to_do = task_to_do

    def execute(self, userdata):
        print("Diferenciando entre tareas de Nahfy...")
        if self.task_to_do.task_type == "NAHFY_DIAGNOSE":
            userdata.task_result = userdata.task_data
            return 'diagnose_task'
        elif self.task_to_do.task_type == "NAHFY_TRANSPORT":
            userdata.task_result = userdata.task_data
            return 'giveitem_task'
        elif self.task_to_do.task_type == "NAHFY_BROADCAST":
            userdata.task_result = userdata.task_data
            return 'chat_task'
        
class Take_Measures(State):
    """
    This class represents the state Take_Measures, and this State will receive some type of measures as input to measure them and uploading it on the server

    Args:
        State (): Represents a moment of our robot in execution
    """
    def __init__(self, task_to_do):
        State.__init__(self, outcomes=['success', 'abort_task'], input_keys=['task_data'], output_keys=['task_result'])
        self.task_to_do = task_to_do

    def execute(self, userdata):
        print("Tomando medidas del paciente...")
        # TODO : This State will be implemented for executing code for taking some measures from sensor implemented in nexts Sprints. We suppose it works correctly
        cont = 0
        while cont<6:
            print(".")
            cont +=1
        if cont == 6:
            success = 'success'
            userdata.task_result = success
            return 'success'
        else:
            success = 'abort_task'
            userdata.task_result = success
            return 'abort_task'

class Activate_Chat(State):
    """
    This class represents the state Activate_Chat, and this State will activate a videochat with the patient and his family or doctor

    Args:
        State (): Represents a moment of our robot in execution
    """
    def __init__(self, task_to_do):
        State.__init__(self, outcomes=['success', 'abort_task'], input_keys=['task_data'], output_keys=['task_result'])
        self.task_to_do = task_to_do

    def execute(self, userdata):
        print("Realizando llamada...")
        # TODO : This State will be implemented for executing code forstarting videochat with the patient. We suppose it works correctly
        cont = 0
        while cont<6:
            print(".")
            cont +=1
        if cont == 6:
            success = 'success'
            userdata.task_result = success
            return 'success'
        else:
            success = 'abort_task'
            userdata.task_result = success
            return 'abort_task'

class Give_Item(State):
    """
    This class represents the state Give_Item, and this State will give the item taken on Pick_Item State to a patient

    Args:
        State (): Represents a moment of our robot in execution
    """
    def __init__(self, task_to_do):
        State.__init__(self, outcomes=['success', 'abort_task'], input_keys=['task_data'], output_keys=['task_result'])
        self.task_to_do = task_to_do

    def execute(self, userdata):
        print("Entregando el item al paciente...")
        # TODO : This State will be implemented for executing code for giving items picked in storage to a patient. We suppose it works correctly
        cont = 0
        while cont<6:
            print(".")
            cont +=1
        if cont == 6:
            success = 'success'
            userdata.task_result = success
            return 'success'
        else:
            success = 'abort_task'
            userdata.task_result = success
            return 'abort_task'

class Go_Point(State):
    """
    This class represents the state Go_Point, and this State will make to come back the robot to the power station on his environment

    Args:
        State (): Represents a moment of our robot in execution
    """
    def __init__(self, task_to_do):
        State.__init__(self, outcomes=['success', 'abort_task'], input_keys=['task_data'], output_keys=['task_result'])
        self.task_to_do = task_to_do

    def execute(self, userdata):
        print("Robot desplazandose...")

        result = mover_robot(self.task_to_do)

        if result == "succes: True":
            success = 'success'
            userdata.task_result = success
            return 'success'
        else:
            success = 'abort_task'
            userdata.task_result = success
            return 'abort_task'
        """
        temp = 1

        if temp == 1:
            success = 'success'
            userdata.task_result = success
            return 'success'
        else:
            success = 'abort_task'
            userdata.task_result = success
            return 'abort_task'
        """

def mover_robot(task):
    """
    Function to move the robot to a goal

    Args:
        task (TaskWork): Object created by us, that contains the details for a specific Task

    Returns:
        string: The value represents the result of the actionlib client
    """
    client = actionlib.SimpleActionClient(task.action_server_name, task.action)
    print("Esperando al servidor...")
    client.wait_for_server()

    #print("GOAL  x: "+str(task.action_goal.x_pos)+" and y: "+str(task.action_goal.y_pos))

    client.send_goal(task.action_goal)
    client.wait_for_result()

    return str(client.get_result())
#! /usr/bin/env python

# robot tasks queue
# --------------------
task_queue = []

# --------------------

TASK_TYPE_NAHFY_MOVE = "NAHFY_MOVE"
TASK_TYPE_NAHFY_DIAGNOSE = "NAHFY_DIAGNOSE"
TASK_TYPE_NAHFY_TRANSPORT = "NAHFY_TRANSPORT"
TASK_TYPE_NAHFY_BROADCAST = "NAHFY_BROADCAST"

TASK_SERVERS = {TASK_TYPE_NAHFY_MOVE: "move_action_server", TASK_TYPE_NAHFY_DIAGNOSE: "diagnose_action_server", TASK_TYPE_NAHFY_TRANSPORT: "transport_action_server", TASK_TYPE_NAHFY_BROADCAST: "broadcast_action_server"}
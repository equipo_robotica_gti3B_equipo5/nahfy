#! /usr/bin/env python

import rospy
from nahfy_work_task_msg.srv import WorkTask, WorkTaskResponse

from task import *
import task_manager as tm
import actionlib  # contiene la clase SimpleActionServer

from nahfy_move_action_msg.msg import MoveAction, MoveGoal, MoveResult
from nahfy_fsm import *

class TaskServer():
    """ Task Server, manage task FIFO queue for the robot

    """

    ctrl_c = False 
    processed_queue = [] # Tasks in order to be processed


    def __init__(self):
        self.task_server = rospy.Service('/task_server', WorkTask, self.task_callback)  # Service creation
        
        rospy.init_node('nahfy_task_server')  # Node start
        rospy.on_shutdown(self.shutdownhook)
        
        self.rate = rospy.Rate(0.5)
        self.read_loop()


    def task_callback(self, request):
        """ Service callback, adds task to the queue
        """
        rospy.loginfo("The service task_server has been called")

        task = self.get_input_task(request)
        tm.task_queue.append(task)

        print("Callback tasks: "+str(len(tm.task_queue)))

        response = WorkTaskResponse()
        response.success = True

        return response    


    def read_loop(self):
        """Infinite loop to check for new tasks entering the queue
            process the tasks and send to its servers and clean the task queue
        """
        
        while not self.ctrl_c:
            print("reading for new tasks... "+str(len(tm.task_queue)))
            if len(tm.task_queue)>0: # Send found tasks to processed queue and clean the common queue
                for task in tm.task_queue:
                    self.processed_queue.append(task)
                tm.task_queue = [] # Clear the queue
            
            if(len(self.processed_queue)>0):
                for i in range(0, len(self.processed_queue)):# For each task in the processed queue
                    task = self.processed_queue[i]

                    #TODO: Process Task depending on the type inside the FSM
                    #print(self.processed_queue[i].task_type)
                    
                    start_fsm = Init_FSM(task) # FSM Start

                    while start_fsm.success == None:
                        #Executing FSM...
                        if start_fsm.success == "success":
                            self.processed_queue.pop(i)
                            break
                        elif start_fsm.success == "aborted":
                            print("Ha ocurrido un error")
                            break

                    #self.processed_queue.pop(i)
                self.processed_queue = []
            
            self.rate.sleep()


    def shutdownhook(self):
        """Stops the server
        """
        print("stoping node task_reader")
        self.ctrl_c = True


    def get_input_task(self, request):
        """Generates the task to process by the task reader
            depndeing on the request type and params its build as a Task object
        Args:
            request (nahfy_task_server.srv.Task): Task service message request, including type and until 5 params with necessary data
        Returns:
            [task.Task]: Task Object with the correct params to send a goal to the correct client
        """
      
        task = None
        if(request.type == tm.TASK_TYPE_NAHFY_MOVE):
            task = RobotTaskMovement(float(request.param_1), float(request.param_2))
        if(request.type == tm.TASK_TYPE_NAHFY_DIAGNOSE):
            task = RobotTaskDiagnose()
        if(request.type == tm.TASK_TYPE_NAHFY_TRANSPORT):
            task = RobotTaskTransport(str(request.param_5), float(request.param_1), float(request.param_2), int(request.param_3), int(request.param_4))
        if(request.type == tm.TASK_TYPE_NAHFY_BROADCAST):
            task = RobotTaskBroadcast()

        return task



if __name__ == '__main__':

    server = TaskServer() # server start

    rospy.loginfo("Service /task_server is ready")
    rospy.spin()  # holds service open
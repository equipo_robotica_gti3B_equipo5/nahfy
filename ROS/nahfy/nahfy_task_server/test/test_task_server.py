#! /usr/bin/env python

from nahfy_task_server.task_server import TaskServer
from nahfy_work_task_msg.srv import WorkTask, WorkTaskRequest


from task import *
import task_manager as tm
import actionlib  # contiene la clase SimpleActionServer

from nahfy_move_action.msg import MoveAction, MoveGoal, MoveResult

import rospy
import rosunit
import unittest
import rostest
PKG = 'nahfy_task_server'
NAME = 'nahfy_task_server_test'


class TestTaskServer(unittest.TestCase):

    def test_task_server(self):

        rospy.wait_for_service('/task_server')
        s = rospy.ServiceProxy('/task_server', WorkTask)

        tests = ["NAHFY_MOVE", "NAHFY_DIAGNOSE", "NAHFY_TRANSPORT", "NAHFY_BROADCAST"]

        for work_task_type in tests:
            resp = s.call(WorkTaskRequest(str(work_task_type), float(0.4), float(4.0), float(0.0), float(0.0), str("")))

            self.assertEqual(resp.success, True)



if __name__ == '__main__':
    rostest.rosrun(PKG, NAME, TestTaskServer)


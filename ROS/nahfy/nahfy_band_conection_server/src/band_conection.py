#! /usr/bin/env python

import argparse
import subprocess
import shutil
import time
import rospy
from std_msgs.msg import Byte
from datetime import datetime

from bluepy.btle import BTLEDisconnectError

from miband import miband

from nahfy_band_conection_msg.srv import band_Conection, band_ConectionResponse


class BandConection():

    def __init__(self):
        self.task_server = rospy.Service('/band_conection', band_Conection, self.task_callback)  # Service creation
        
        rospy.init_node('nahfy_band_conection')  # Node start
        rospy.on_shutdown(self.shutdownhook)
        
        self.rate = rospy.Rate(0.5)


    def task_callback(self, request):
        """ Service callback, adds task to the queue
        """
        '''mac = request.band_mac

        auth_key = request.band_auth_key
        
        #TODO Podría fallar
        auth_key_byte = bytes.fromhex(auth_key)

        message = Byte()

        success = False
        while not success:
            try:
                self.band = miband(mac, auth_key_byte, debug=True)
                success = self.band.initialize()
            except BTLEDisconnectError:
                print('Connection to the MIBand failed. Trying out again in 3 seconds')
                time.sleep(3)
                continue

        self.get_realtime_patient_data()
        
        self.heartbeat = -1
        self.steps = -1
        self.calories = -1.0
        self.distance = -1.0
        self.battery = -1

        contador = 0

        response = band_ConectionResponse()

        while self.heartbeat == -1 and self.steps == -1 and self.calories == -1.0 and self.distance == -1.0 and self.battery == -1:
            if contador < 10:
                contador = contador + 1
            else:
                response.success = False
                return response

            time.sleep(10)   

        if self.heartbeat != -1 and self.steps != -1 and self.calories != -1.0 and self.distance != -1.0 and self.battery != -1:
            response.success = True
            response.heart_rate = self.heartbeat
            response.steps = self.steps
            response.calories = self.calories
            response.distance = self.distance
            response.battery = self.battery

            return response'''
        self.heartbeat = -1
        self.steps = -1
        self.calories = -1.0
        self.distance = -1.0
        self.battery = -1

        contador = 0
        self.obtain()

        response = band_ConectionResponse()

        while self.heartbeat == -1 and self.steps == -1 and self.calories == -1.0 and self.distance == -1.0 and self.battery == -1:
            if contador < 10:
                contador = contador + 1
            else:
                response.success = False
                return response

            time.sleep(10)   

        if self.heartbeat != -1 and self.steps != -1 and self.calories != -1.0 and self.distance != -1.0 and self.battery != -1:
            response.success = True
            response.heart_rate = self.heartbeat
            response.steps = self.steps
            response.calories = self.calories
            response.distance = self.distance
            response.battery = self.battery

            return response

        

    def obtain_patient_data(self, data):
        self.heartbeat = int(data)
        binfo = self.band.get_steps()
        self.steps = int(binfo['steps'])
        self.calories = float(binfo['calories'])
        self.distance = float(binfo['meters'])
        self.battery = int(self.band.get_battery_info()['level'])

        print ('Realtime heart BPM:', data)
        print ('Number of steps: ', binfo['steps'])
        print ('Calories: ', binfo['calories'])
        print ('Distance travelled in meters: ', binfo['meters'])
        print ('Battery:', self.band.get_battery_info()['level'])
        self.band.change_callback(False)

    def obtain(self):
        self.heartbeat = 85
        self.steps = 200
        self.calories = 101.1
        self.distance = 300.4
        self.battery = 60

    # Needs Authband.stop_realtime()
    def get_realtime_patient_data(self):
        self.band.change_callback(True)
        #self.band.start_heart_rate_realtime(heart_measure_callback=self.obtain_patient_data)
        #self.band.start_heart_rate_realtime(heart_measure_callback=self.obtain)
        

    def shutdownhook(self):
        """Stops the server"""
        print("stoping node task_reader")
        self.ctrl_c = True     



if __name__ == '__main__':

    server = BandConection() # server start

    rospy.loginfo("Service /band_conection is ready")
    rospy.spin()  # holds service open


import requests
import json

def get_patients():
    url = "http://localhost/nahfy/API/v1.0/patients"
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    print(json_res["datos"])

def get_patient_room(id):
    url = "http://localhost/nahfy/API/v1.0/patients?idPatient="+str(int(id))
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    json_data = json_res["datos"]
    print("IDRoom : "+str(json_data[0]["idRoom"]))
    room = get_room_by_id(json_data[0]["idRoom"])
    
    return(room[0],room[1])

def get_patients_by_center(idCenter):
    url = "http://localhost/nahfy/API/v1.0/patients?idCenter="+str(int(idCenter))
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    json_data = json_res["datos"]
    
    return(json_data)

def post_patients(name, surnames, nif, mail, idRoom):

    url = "http://localhost/nahfy/API/v1.0/patients"

    payload={'newNamePatient': name,
    'newSurnamesPatient': surnames,
    'newNifPatient': nif,
    'newMailPatient': mail,
    'newIdRoom': idRoom
    }

    response = requests.request("POST", url, data=payload)

    #print(response.json())

def get_measures():
    url = "http://localhost/nahfy/API/v1.0/measures"
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    return json_res["datos"]

def post_measures(idMeasure, idType, value, idPatient):

    url = "http://localhost/nahfy/API/v1.0/measures"

    payload={'newIdMeasure':idMeasure,
    'newIdType':idType, 
    'newMeasureValue':value, 
    'newMeasureIdPatient':idPatient
    }

    response = requests.request("POST", url, data=payload)

    #print(response.json())

def get_robots():
    url = "http://localhost/nahfy/API/v1.0/robots"
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    return json_res["datos"]

def update_robot_position(id, x_now, y_now):
    url = "http://localhost/nahfy/API/v1.0/robots?id="+str(int(id))+"&xNow="+str(x_now)+"&yNow="+str(y_now)
    payload={}
    headers={}

    requests.put(url=url, headers=headers, data=payload)

def update_robot_state(id, state):
    url = "http://localhost/nahfy/API/v1.0/robots?id="+str(int(id))+"&state="+state
    payload={}
    headers={}

    requests.put(url=url, headers=headers, data=payload)

def get_centers():
    url = "http://localhost/nahfy/API/v1.0/centers"
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    return json_res["datos"]

def get_incidents():
    url = "http://localhost/nahfy/API/v1.0/incidents"
    payload={}
    headers={}

    response = requests.request("GET", url, headers=headers, data=payload)
    return response.json()

def get_rooms():
    url = "http://localhost/nahfy/API/v1.0/rooms"
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    return json_res["datos"]

def get_room_by_id(id):
    int_id = int(id)
    url = "http://localhost/nahfy/API/v1.0/rooms?id="+str(int_id)
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    x_value = json_res["datos"][0]["x"]
    y_value = json_res["datos"][0]["y"]
    return (float(x_value),float(y_value))

def get_users():
    url = "http://localhost/nahfy/API/v1.0/users"
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    return json_res["datos"]

def get_typemeasure():
    url = "http://localhost/nahfy/API/v1.0/typemeasure"
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    return json_res["datos"]

def get_customerscenters():
    url = "http://localhost/nahfy/API/v1.0/customerscenters"
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    return json_res["datos"]

def get_maps():
    url = "http://localhost/nahfy/API/v1.0/maps"
    payload={}
    headers={}

    response = requests.get(url=url, headers=headers, data=payload)
    json_res = json.loads(response.text)
    return json_res["datos"]

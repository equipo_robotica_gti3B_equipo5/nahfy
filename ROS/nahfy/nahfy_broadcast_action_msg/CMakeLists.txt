cmake_minimum_required(VERSION 3.0.2)
project(nahfy_broadcast_action_msg)


find_package(catkin REQUIRED COMPONENTS
  rospy
  actionlib_msgs
)


################################################
## Declare ROS messages, services and actions ##
################################################



## Generate actions in the 'action' folder
add_action_files(
  DIRECTORY action
  FILES Broadcast.action
 )


## Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  std_msgs  # Or other packages containing msgs
  actionlib_msgs
)


###################################
## catkin specific configuration ##
###################################

catkin_package(
  CATKIN_DEPENDS rospy
  actionlib_msgs
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
# include
  ${catkin_INCLUDE_DIRS}
)

#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Byte.h>
#include <ros/time.h>
#include <sensor_msgs/Range.h>



#include <Servo.h> //Setepper motor control library (servo library)


#define SERVO_PIN 5 // Servo digital pin

#define DIST_ECHO_PIN 6
#define DIST_TRIGGER_PIN 7


#define SERVO_OPEN_POS 20
#define SERVO_CLOSE_POS 150

// Servo and servo current position
Servo servo_futaba; 
int servo_pos; 
long range_time;

bool lid_open = false;
bool wait_for_cargo = false;
float lastDistance = 0.00f;
const float cargo_th_put = 5.5f;
const float cargo_th_remove = 6.0f;


sensor_msgs::Range range_msg; // Range to measure distance
ros::Publisher pub_range( "/nahfy_distance", &range_msg);
char frameid[] = "/nahfy_distance";



ros::NodeHandle  nh; // Node Handler

/**
 * Servo open lid callback and wait until cargo is detected
 */
void servo_open_for_cargo_cb( const std_msgs::Byte& open_params) {
    
    servo_futaba.write(SERVO_OPEN_POS);
    delay(15); 

    delay(500);
    lid_open = true;
    wait_for_cargo = true;
  
}


/**
 * Servo open lid callback and wait until cargo is removed
 */
void servo_open_for_wait_cb( const std_msgs::Byte& open_params) {
    
    servo_open();

    delay(500);
    lid_open = true;
    wait_for_cargo = false;
  
}


/**
 * Servo close lid callback
 */
void servo_close_cb( const std_msgs::Byte& close_params) {
    
    servo_close();

    delay(500);
    lid_open= false;
    wait_for_cargo = false;
}


void servo_close(){
    servo_futaba.write(SERVO_CLOSE_POS);
    delay(15); 
}

void servo_open(){
    servo_futaba.write(SERVO_OPEN_POS);
    delay(15); 
}



/**
 * Controls the lid wait and close
 * depending on the robot task status
 */
void lid_waitter_control(){

  readDistance();
  if(wait_for_cargo){
    if(lastDistance < cargo_th_put){

      delay(3000);
      servo_close();
    }
    
  }else{
    if(lastDistance > cargo_th_remove){
      // CLOSE LID AND COMPLETED
      servo_close();
    }

    
  }
  
}


/**
 * microseconds to cm for the sensor used sc-hr04
 */
float microsecondsToCentimeters(float microseconds) {
  return microseconds / 58.3;
}


/**
 * Retrieve distance requesting to the sensor
 */
float getRange_Ultrasound(int echo_pin, int trigger_pin) {
  float duration, cm;
  digitalWrite(trigger_pin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigger_pin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger_pin, LOW);
  duration = pulseIn(echo_pin, HIGH);
  cm = microsecondsToCentimeters(duration);
  return cm;
}


void createDistanceSensor(){
  range_msg.radiation_type = sensor_msgs::Range::ULTRASOUND;
  range_msg.header.frame_id =  frameid;
  range_msg.field_of_view = 0.1;  
  range_msg.min_range = 0.0;
  range_msg.max_range = 6.47;

  pinMode(DIST_TRIGGER_PIN, OUTPUT);
  pinMode(DIST_ECHO_PIN, INPUT);
}


void readDistance(){

  
  //publish the echo value every 50 milliseconds
  //since it takes that long for the sensor to stablize
  if ( millis() >= range_time ) {
    int r = 0;

    lastDistance = getRange_Ultrasound(DIST_ECHO_PIN, DIST_TRIGGER_PIN);
    range_msg.range = lastDistance;
    range_msg.header.stamp = nh.now();
    pub_range.publish(&range_msg);
    range_time =  millis() + 1000;
  }

  delay(500);

}



// SUBSCRIBERS


ros::Subscriber<std_msgs::Byte> servo_open_for_cargo_sub("servo_open_for_cargo", servo_open_for_cargo_cb);
ros::Subscriber<std_msgs::Byte> servo_open_for_wait_sub("servo_open_for_wait", servo_open_for_wait_cb);
ros::Subscriber<std_msgs::Byte> servo_close_sub("servo_close", servo_close_cb );



// POLLING FUNCTIONS


void setup() {
  
  // Attach and reset the servo
  servo_futaba.attach(SERVO_PIN);
  servo_futaba.write(150);
  

  // Start node and subscribe
  nh.initNode();


  // Subscribers
  nh.subscribe(servo_open_for_cargo_sub);
  nh.subscribe(servo_open_for_wait_sub);
  nh.subscribe(servo_close_sub);

  // Publushers
  nh.advertise(pub_range);
  createDistanceSensor();

}

void loop() {
  nh.spinOnce();


  if(lid_open){
    lid_waitter_control();
  }
  
}

// Server API directions
const servidorAPI = "http://localhost/nahfy/API/v1.0/";
const servidor = "http://localhost/";

// Holds ROS connection
data = {
    // ros connection
    ros: null,
    rosbridge_address: 'ws://127.0.0.1:9090/',
    connected: false,
}

// Type of incidences of robot
const low_energy = "Bateria baja";
const not_energy = "Sin bateria";
const not_enable = "No disponible";

// Real coordinates
/*const storage_position = [0,0]
const reception_position = [-0.28,1.52]
const corridor_position = [0.39,0.84]
const room1_position = [1.31,-0.27]
const bathroom1_position = [0.934,0.15]
const room2_position = [1.31,0.74]
const bathroom2_position = [0.812,1.05]
const room3_position = [1.1,1.72]
const bathroom3_position = [0.62,2.05]*/

// Fake coordinates
/*const storage_position = [0,0]
const reception_position = [0.635,6.16]
const corridor_position = [-3.42,2.45]
const room1_position = [-8.29,-1.64]
const bathroom1_position = [-5.79,-1.2]
const room2_position = [-6.77,0.93]
const bathroom2_position = [-5.19,3.32]
const room3_position = [-7.39,6.87]
const bathroom3_position = [-5.22,6.82]*/
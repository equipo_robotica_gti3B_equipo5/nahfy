function check_is_loged(){
    var cookie = get_cookie("nahfy_user");
    if(cookie.length>=1){
        var type_client_cookie = get_cookie("nahfy_rol");
        if(type_client_cookie == 1){
            if(window.location.href.includes("login.html")){
                go_admin_panel();
            }
        }else{
            if(window.location.href.includes("login.html")){
                go_client_panel()
            }
        }
    }else{
        if(!window.location.href.includes("login.html")){
            go_login();
        }
    }
}

function check_send_mail(){
    if(document.getElementById("email-restore").value == ""){
        document.getElementById("error-empty-restore").style.display = "block";
    }else{
        document.getElementById("error-empty-restore").style.display = "none";
        // TODO: RELLENAR
    }
}

function check_login(){
    var mail = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    if(mail == "" || password == ""){
        document.getElementById("error-login-empty").style.display = "block";
    }else{
        document.getElementById("error-login-empty").style.display = "none";
        // BD query
        get_init_session(mail,password);
    }
}

function get_init_session(mail, password){
    var parameters = "?email="+mail+"&password="+password;
    
    var request = servidorAPI+"login/"+parameters; // Resource to acces to all users
    
    fetch(request, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // The API response is returned in JSON format
    }).then(function(data){        
        // The returned data is obtained and passed to the processing function
        init_session(data["datos"]);
        console.log(data);
    });
}

function init_session(data){
    if(data.length>0){
        var usuario = data[0];
        console.log(usuario);
        var isAdmin = usuario["isAdmin"];

        set_cookie("nahfy_user",usuario["email"],2);
        set_cookie("nahfy_password",usuario["password"],2);
        set_cookie("nahfy_id",usuario["id"],2);
        set_cookie("nahfy_rol",isAdmin,2);
        if((isAdmin)==1){
            go_admin_panel();
        } else{
            go_client_panel();
        }
        
    } else {
        document.getElementById("error-login").style.display="block";
    }
}

function go_admin_panel(){
    window.location.href="admin.html";
}

function go_client_panel(){
    window.location.href="control_robot.html";
}

function go_login(){
    window.location.href="login.html";
}

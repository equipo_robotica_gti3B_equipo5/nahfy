var last_movement_input_checked = "coordinates"
var coordinates_x = new Map(); var coordinates_y = new Map();

/*-----------------------------------------------------------------------------------
---------------------------------ROBOTS----------------------------------------------
-----------------------------------------------------------------------------------*/

function getRobots(idMap){

    var recurse = servidorAPI + "robots/?idMap="+idMap+""; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        getRobotsPatients(data);

    });
}

function getRobotsPatients(dataRobots){

    var recurse = servidorAPI + "incidents/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (dataAlerts) {
        createRobotList(dataRobots["datos"],dataAlerts["datos"]);

    });
}

function createRobotList(robots,incidents){
    var container = document.getElementById("robots-list");
    var view = ``;
    for(var robot of robots){
        view += `
        <div id="robots-card-div">
            <div id="robots-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-2 justify-flex">
                            <img class="image-inherit" src="media/images/${robot["image"]}">
                        </div>
                        <div class="col-10 col-lg-7 col-md-10 justify-flex">
                            <h6><b> ${robot["name"]}</b></h6>
                            <p><b>Estado actual: </b>${robot["state"]}</p>
                        </div>
                        <div class="col-lg-3 col-md-12 justify-flex robots-list-btn">
                            <button class="btn btn-primary" id="btn_connect_ros" onclick="connect()">Conectar robot</button>
                            <div id="incidents-robot-${robot["id"]}" class="container-alerts-robots-icon"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
    };

    container.innerHTML = view;
    
    var incidentContainer = document.getElementById("incidents-robot-"+robot["id"]);

    for(var robot of robots){
        for(var incident of incidents){
            if(robot["id"] == incident["idRobot"]){
                switch(incident["problem"]){
                    case low_energy:
                        incidentContainer.innerHTML += `<i class="fas fa-battery-quarter"></i>`;
                        break;
                    case not_energy:
                        incidentContainer.innerHTML += `<i class="fas fa-battery-empty"></i>`;
                    break;
                    case not_enable:
                        incidentContainer.innerHTML += `<i class="fas fa-ban"></i>`;
                        break;
                }
            }
        }
    }
}

/*-----------------------------------------------------------------------------------
---------------------------------MAPS----------------------------------------------
-----------------------------------------------------------------------------------*/

function getMaps(){
    var idClient = getClientId()

    var recurse = servidorAPI + "maps/?id="+idClient+""; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        fillDropdownMaps(data["datos"]);
    });
}

function showFirstMap(map){
    changeMap(map["name"],map["plan"],map["id"]);
    getRobots(map["id"]);
}

function fillDropdownMaps(maps){
    var view = ``;
    var container = document.getElementById("dropdown_floors");
    var isFirtsMap = true;
    for(var map of maps){
        if(isFirtsMap){
            showFirstMap(map);
            isFirtsMap = false;
        }
        view += `<li onclick="changeMap('${map["name"]}','${map["plan"]}','${map["id"]}')"><a href="#">${map["name"]}</a></li>`;
    }
    container.innerHTML = view;
}

function changeMap(mapName, mapImage, id){
    var container = document.getElementById("map-image-container");
    var view = `<img alt="mapa" id="map-image" src="./media/images/`+mapImage+`">`;
    container.innerHTML = view;

    var container = document.getElementById("dropdown-map-btn");
    var view = ``+mapName+` <span class="caret"></span>`;
    container.innerHTML = view;

    getRooms(id);
    getRobots(id);
    getMeasuresType();
}

/*-----------------------------------------------------------------------------------
-------------------------------PATIENTS----------------------------------------------
-----------------------------------------------------------------------------------*/
function getPatients(plant){
    var recurse = servidorAPI + "patients/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        putPatientsIntoDiagnose(data["datos"], plant);
    });
}

function putPatientsIntoDiagnose(data, plant){
    var container = document.getElementById("options-patients");
    var container2 = document.getElementById("options-patients-transport");

    var view = ``;
    var view2 = ``;

    for(var patient of data){
        if(plant == patient["idMap"]){
            view += `<label><input type="checkbox" name="checkboxPatients[]" value="${patient["id"]}" onclick="togglePatients(this)"/>${patient["name"]}</label>`;
            view2 += `<label><input type="checkbox" name="checkboxPatientsTransport[]" value="${patient["id"]}" onclick="togglePatientsTransport(this)"/>${patient["name"]}</label>`;
        }
    };

    if(view == ``){
        view = "No hay tipos de pacientes";
        view2 = "No hay tipos de pacientes";
    }

    container.innerHTML = view;
    container2.innerHTML = view2;
}

function toggleAllPatients(source){
    var checkboxes = document.getElementsByName('checkboxPatients[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
    }

    checkBtnCanEnable("checkboxPatients[]","checkboxMeasuresType[]","btn-diagnose");
}

function togglePatients(source){
    if(document.getElementById("allPatients").checked){
        if(!source.checked){
            document.getElementById("allPatients").checked = false
        }
    }

    checkBtnCanEnable("checkboxPatients[]","checkboxMeasuresType[]","btn-diagnose");
}

function toggleAllPatientsTransport(source){
    var checkboxes = document.getElementsByName('checkboxPatientsTransport[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
    }

    checkBtnCanEnable("checkboxPatientsTransport[]","checkboxRoomsTransport[]","btn-transport");
}

function togglePatientsTransport(source){
    if(document.getElementById("allPatientsTransport").checked){
        if(!source.checked){
            document.getElementById("allPatientsTransport").checked = false
        }
    }

    checkBtnCanEnable("checkboxPatientsTransport[]","checkboxRoomsTransport[]","btn-transport");
}


/*-----------------------------------------------------------------------------------
-------------------------------MEASURES TYPE-----------------------------------------
-----------------------------------------------------------------------------------*/
function getMeasuresType(){
    var recurse = servidorAPI + "typemeasure/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        putMeasuresTypeIntoDiagnose(data["datos"]);
    });
}

function putMeasuresTypeIntoDiagnose(data){
    var container = document.getElementById("options-measure-type");
    var view = ``;

    for(var measureType of data){
        view += `<label><input name="checkboxMeasuresType[]" type="checkbox" value="${measureType["id"]}" onclick="toggleMeasure(this)"/>${measureType["name"]}</label>`;
    };

    if(view == ``){
        view = "No hay ningun tipo de medidas";
    }

    container.innerHTML = view;
}

function toggleMeasure(source){
    if(document.getElementById("allMeasures").checked){
        if(!source.checked){
            document.getElementById("allMeasures").checked = false
        }
    }

    checkBtnCanEnable("checkboxPatients[]","checkboxMeasuresType[]","btn-diagnose");
}

function toggleAllMeasures(source){
    var checkboxes = document.getElementsByName('checkboxMeasuresType[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
    }

    checkBtnCanEnable("checkboxPatients[]","checkboxMeasuresType[]","btn-diagnose");
}

/*-----------------------------------------------------------------------------------
----------------------------------ROOMS----------------------------------------------
-----------------------------------------------------------------------------------*/

function getRooms(idMap){
    var recurse = servidorAPI + "rooms/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        putRoomsIntoMoveRobot(data["datos"], idMap);
    });
}

function putRoomsIntoMoveRobot(data, id){
    var container = document.getElementById("options-move");
    var container2 = document.getElementById("options-rooms-transport");
    var view = ``;
    var view2 = ``;

    coordinates_x = new Map(); 
    coordinates_y = new Map();

    getPatients(id);

    for(var room of data){
        if(room["idMap"] == id){
            view += `<label><input type="checkbox" name="checkboxRooms[]" value="${room["room"]}" onclick="toggleRooms(this)"/>${room["room"]}</label>`;
            view2 += `<label><input type="checkbox" name="checkboxRoomsTransport[]" value="${room["room"]}" onclick="toggleRoomsTransport(this)"/>${room["room"]}</label>`;
            coordinates_x.set(room["room"],parseFloat(room["x"]).toFixed(2));
            coordinates_y.set(room["room"],parseFloat(room["y"]).toFixed(2));
        }
    };

    if(view == ``){
        view = "No hay habitaciones para este mapa";
        view2 = "No hay habitaciones para este mapa";
    }

    container.innerHTML = view;
    container2.innerHTML = view2;
}

function toggleRooms(checkbox){
    var checkboxes = document.getElementsByName('checkboxRooms[]')
    checkboxes.forEach((item) => {
        if (item !== checkbox) item.checked = false
    })
}

function toggleRoomsTransport(source){
    if(document.getElementById("allRoomsTransport").checked){
        if(!source.checked){
            document.getElementById("allRoomsTransport").checked = false
        }
    }

    checkBtnCanEnable("checkboxPatientsTransport[]","checkboxRoomsTransport[]","btn-transport");
}

function toggleAllRoomsTransport(source){
    console.log("dentro")
    var checkboxes = document.getElementsByName('checkboxRoomsTransport[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
    }

    checkBtnCanEnable("checkboxPatientsTransport[]","checkboxRoomsTransport[]","btn-transport");
}

/*-----------------------------------------------------------------------------------
-----------------------------PATIENT ALERTS------------------------------------------
-----------------------------------------------------------------------------------*/

function getPatientAlert(){

    var recurse = servidorAPI + "incidentsPatient/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        var container = document.getElementById("patient-alerts");
        var view = ``;

        // Catch the data and give it a other function
        
        view = createPatientAlert(data["datos"], view);
        container.innerHTML = view;

    });
}

function createPatientAlert(alerts, view){
    
    for(var alert of alerts){
        view += `
        <div id="patient-alerts-card-div">
            <div id="patient-alert-color" style="background-color: ${alert["color"]};">
            </div>
            <div id="patient-alerts-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-2 col-lg-2 col-md-6 justify-flex">
                            <img class="image-inherit" src="media/images/${alert["image"]}">
                        </div>
                        <div class="col-10 col-lg-6 col-md-6 justify-flex">
                            <h6><b> ${alert["name"]}</b></h6>
                            <p><b> Problema:</b> ${alert["problem"]}</p>
                            <p style="margin-bottom: revert;"><b> Habitación:</b>  ${alert["room"]}</p>
                        </div>
                        <div class="col-lg-4 col-md-12 justify-flex robots-list-btn">
                            <button class="btn btn-primary">Enviar robot</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
      
    };
    return view
   
}

/*-----------------------------------------------------------------------------------
---------------------------------ROS CONNECTION--------------------------------------
-----------------------------------------------------------------------------------*/

/**
 * Connects with ROS through
 * ROS bridge
 */
function connect(){
    data.ros = new ROSLIB.Ros({
        url: data.rosbridge_address
    })

    data.interval = setInterval(() => {
        if (data.ros != null && data.ros.isConnected) {
           data.ros.getNodes((data) => { }, (error) => { })
        }
      }, 10000)

    // Define callbacks
    data.ros.on("connection", () => {
        data.connected = true
        console.log("ROSBridge connected!")
        document.getElementById("alert-cannot-connect-ros").style.display = "none";

        change_btn_connect("connected");

        action_controls = document.getElementsByClassName("ctrl-action");
        for (control of action_controls) {
            control.disabled = false
        }

        document.getElementById('map-image-container').style.display = "none";

        data.mapViewer = new ROS2D.Viewer({
            divID: 'map',
            width: 420,
            height: 360
        })
        setCamera()
        
        //Setup the map client
        data.mapGridClient = new ROS2D.OccupancyGridClient({
                ros : data.ros,
                rootObject : data.mapViewer.scene,
                continuous: true //este atributo permite mantener el mapa actualizado cuando se cambie por el algoritmo de mapeado del robot
        })
        
        //Scale the canvas to fit to the map
        data.mapGridClient.on('change', () => {
                data.mapViewer.scaleToDimensions(data.mapGridClient.currentGrid.width, data.mapGridClient.currentGrid.height)
                data.mapViewer.shift(data.mapGridClient.currentGrid.pose.position.x, data.mapGridClient.currentGrid.pose.position.y)
        })

    })
    data.ros.on("error", (error) => {
        console.log("ROS connection error")
        console.log(error)
        document.getElementById("alert-cannot-connect-ros").style.display = "inherit";
    })
    
    data.ros.on("close", () => {
        data.connected = false
        console.log("ROSBridge connection CLOSED!")

        change_btn_connect("disconnected");

        action_controls = document.getElementsByClassName("ctrl-action");
        for (control of action_controls) {
            control.disabled = true
        }

        document.getElementById('map').innerHTML = "";
        document.getElementById('map-image-container').style.display = "flex";
        getMaps();

        document.getElementById("divCamera").innerHTML = "";
    })
}

function disconnect(){
    data.ros.close()        
    data.connected = false
    
    change_btn_connect("disconnected");

    action_controls = document.getElementsByClassName("ctrl-action");
    for (control of action_controls) {
        control.disabled = true
    }
} 

function change_btn_connect(option){
    var btn = document.getElementById("btn_connect_ros");
    switch(option){
        case "connected":
            btn.className = "btn btn-primary-2";
            btn.innerHTML = "Desconectar robot";
            btn.onclick = disconnect
            break;
        case "disconnected":
            btn.className = "btn btn-primary";
            btn.innerHTML = "Conectar robot";
            btn.onclick = connect
            break;
    }
}

/*-----------------------------------------------------------------------------------
---------------------------------ROS MOVE--------------------------------------------
-----------------------------------------------------------------------------------*/

function action_move() {

    data.service_response = ''

    var coordinates = get_coordinates()

    let x_pos = coordinates[0]
    let y_pos = coordinates[1]

    if(x_pos!=null && y_pos !=null){
        //definimos los datos del servicio
        let move_service = new ROSLIB.Service({
            ros: data.ros,
            name: '/task_server',
            serviceType: 'nahfy_task_server/WorkTask'
        })

        //definimos el parámetro de la llamada
        let request = new ROSLIB.ServiceRequest({
            type: "NAHFY_MOVE",
            param_1: parseFloat(x_pos),
            param_2: parseFloat(y_pos),
            param_3: 0.00,
            param_4: 0.00,
            param_5: ""
        })

        move_service.callService(request, (result) => {
            data.service_busy = false
            data.service_response = JSON.stringify(result) // Parse the result
            console.log(data.service_response)


            console.log("Service sent succesfully")

        }, (error) => {
            data.service_busy = false
            console.error(error) // Print error by console
        })	
    } // end if

}

function get_coordinates(){
    var x = 0;
    var y = 0;

    if(last_movement_input_checked == "coordinates"){
        x = document.getElementById("input_coordinate_x").value
        y = document.getElementById("input_coordinate_y").value
    }else{
        x = coordinates_x.get(last_movement_input_checked);
        y = coordinates_y.get(last_movement_input_checked);
    }

    return [x,y]

}

/*-----------------------------------------------------------------------------------
---------------------------------ROS DIAGNOSE----------------------------------------
-----------------------------------------------------------------------------------*/

function action_diagnose(){

    console.log("diagnosing...")
    data.service_response = ''

    // TODO
    var measures_to_take = getCheckboxNameChecked("checkboxMeasuresType[]");
    var patients_to_take_measures = getCheckboxNameChecked("checkboxPatients[]");

    //definimos los datos del servicio
    let move_service = new ROSLIB.Service({
        ros: data.ros,
        name: '/task_server',
        serviceType: 'nahfy_work_task_msg/WorkTask'
    })

    //definimos el parámetro de la llamada
    let request = new ROSLIB.ServiceRequest({
        type: "NAHFY_DIAGNOSE",
        param_1: 0.00,
        param_2: 0.00,
        param_3: 0.00,
        param_4: 0.00,
        param_5: ""
    })

    move_service.callService(request, (result) => {
        data.service_busy = false
        data.service_response = JSON.stringify(result) // Parse the result
        console.log(data.service_response)


        console.log("Service sent succesfully")

    }, (error) => {
        data.service_busy = false
        console.error(error) // Print error by console
    })	

}

function getCheckboxNameChecked(name){
    var checkbox = document.getElementsByName(name);
    var list = []

    for(var i=0, n=checkbox.length;i<n;i++) {
        var item  = checkbox[i];
        if(item.checked){
            list.push(item.value);
        }
    }

    return list;
}

/*-----------------------------------------------------------------------------------
---------------------------------ROS TRANSPORT---------------------------------------
-----------------------------------------------------------------------------------*/

function action_transport(){
    data.service_response = ''

    var rooms_to_transport = getCheckboxNameChecked("checkboxRoomsTransport[]");
    var patients_to_transport = getCheckboxNameChecked("checkboxPatientsTransport[]");

    //definimos los datos del servicio
    let move_service = new ROSLIB.Service({
        ros: data.ros,
        name: '/task_server',
        serviceType: 'nahfy_task_server/WorkTask'
    })

    //definimos el parámetro de la llamada
    let request = new ROSLIB.ServiceRequest({
        type: "NAHFY_TRANSPORT",
        param_1: 0.00,
        param_2: 0.00,
        param_3: 10,
        param_4: 1,
        param_5: "patient"
    })

    move_service.callService(request, (result) => {
        data.service_busy = false
        data.service_response = JSON.stringify(result) // Parse the result
        console.log(data.service_response)


        console.log("Service sent succesfully")

    }, (error) => {
        data.service_busy = false
        console.error(error) // Print error by console
    })	
}

/*-----------------------------------------------------------------------------------
---------------------------------ROS BROADCAST---------------------------------------
-----------------------------------------------------------------------------------*/

function action_broadcast(){
    data.service_response = ''

    //definimos los datos del servicio
    let move_service = new ROSLIB.Service({
        ros: data.ros,
        name: '/task_server',
        serviceType: 'nahfy_task_server/WorkTask'
    })

    //definimos el parámetro de la llamada
    let request = new ROSLIB.ServiceRequest({
        type: "NAHFY_BROADCAST",
        param_1: 0.00,
        param_2: 0.00,
        param_3: 0.00,
        param_4: 0.00,
        param_5: ""
    })

    move_service.callService(request, (result) => {
        data.service_busy = false
        data.service_response = JSON.stringify(result) // Parse the result
        console.log(data.service_response)


        console.log("Service sent succesfully")

    }, (error) => {
        data.service_busy = false
        console.error(error) // Print error by console
    })	

}  

/*-----------------------------------------------------------------------------------
-----------------------------------OTHERS--------------------------------------------
-----------------------------------------------------------------------------------*/

function getClientId(){
    var id = get_cookie("nahfy_id");
    return id;
}

$('.dropdown-menu a').on('click', function(){    
    $('.dropdown-toggle').html($(this).html() + '<span class="caret"></span>');    
})

function checkBtnCanEnable(name1, name2, btnId){
    var checkbox1 = document.getElementsByName(name1);
    var checkbox2 = document.getElementsByName(name2);

    var count1 = 0;
    var count2 = 0;

    for(var i=0, n=checkbox1.length;i<n;i++) {
        if(checkbox1[i].checked){
            count1++;
        }
    }

    for(var i=0, n=checkbox2.length;i<n;i++) {
        if(checkbox2[i].checked){
            count2++;
        }
    }

    var btn = document.getElementById(btnId);

    if(btnId == "btn-transport"){
        if(count1 > 0 || count2 > 0){
            btn.disabled = false;
        }else{
            btn.disabled = true;
        }
    }else{
        if(count1 > 0 && count2 > 0){
            btn.disabled = false;
        }else{
            btn.disabled = true;
        }
    }

}

function setCamera(){
	console.log("setting the camera")
	let viewer1 = new MJPEGCANVAS.Viewer({
	    divID: "divCamera", //elemento del html donde mostraremos la cámara
	    host: "127.0.0.1:8080", //dirección del servidor de vídeo
	    width: 450, //no pongas un tamaño mucho mayor porque puede dar error
	    height: 550,
        //simulacion
	    topic: "/turtlebot3/camera/image_raw",
        //real
        //topic:"/raspicam_node/image",
	    ssl: false,
	})
}
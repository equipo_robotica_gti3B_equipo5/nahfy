
document.addEventListener('DOMContentLoaded', event => {
    console.log("entro en la pagina")


    // Add Event listeners to elements
    document.getElementById("btn_connect_ros").addEventListener("click", connect)
    document.getElementById("btn_disconnect_ros").addEventListener("click", disconnect)
    
    document.getElementById("btn_move_ros").addEventListener("click", action_move)
    document.getElementById("btn_recognice_ros").addEventListener("click", action_diagnose)
    document.getElementById("btn_transport_ros").addEventListener("click", action_transport)
    document.getElementById("btn_broadcast_ros").addEventListener("click", action_broadcast)


    // Holds ROS connection
    data = {
        // ros connection
        ros: null,
        rosbridge_address: 'ws://127.0.0.1:9090/',
        connected: false,
    }

    
    /**
     * Connects with ROS through
     * ROS bridge
     */
    function connect(){
        
        data.ros = new ROSLIB.Ros({
              url: data.rosbridge_address
      })

      // Define callbacks
      data.ros.on("connection", () => {
          data.connected = true
          console.log("ROSBridge connected!")
          document.getElementById("lbl_ros_connection").innerHTML = "ROS Connected!"

        action_controls = document.getElementsByClassName("ctrl-action");
        for (control of action_controls) {
            control.disabled = false
        }

      })
      data.ros.on("error", (error) => {
          console.log("ROS connection error")
          console.log(error)
      })
      data.ros.on("close", () => {
          data.connected = false
          console.log("ROSBridge connection CLOSED!")
          document.getElementById("lbl_ros_connection").innerHTML = "Not connected"

          action_controls = document.getElementsByClassName("ctrl-action");
          for (control of action_controls) {
              control.disabled = true
          }
  

      })
  }





  function action_move() {

    data.service_response = ''

    let x_pos = document.getElementById("input_coordinate_x").value
    let y_pos = document.getElementById("input_coordinate_y").value

    if(x_pos!=null && y_pos !=null){
        //definimos los datos del servicio
        let move_service = new ROSLIB.Service({
            ros: data.ros,
            name: '/task_server',
            serviceType: 'nahfy_task_server/Task'
        })

        //definimos el parámetro de la llamada
        let request = new ROSLIB.ServiceRequest({
            type: "NAHFY_MOVE",
            param_1: parseFloat(x_pos),
            param_2: parseFloat(y_pos),
            param_3: 0.00,
            param_4: 0.00,
            param_5: ""
        })

        move_service.callService(request, (result) => {
            data.service_busy = false
            data.service_response = JSON.stringify(result) // Parse the result
            console.log(data.service_response)


            console.log("Service sent succesfully")

        }, (error) => {
            data.service_busy = false
            console.error(error) // Print error by console
        })	
    } // end if

  }


  function action_diagnose(){
    data.service_response = ''

        //definimos los datos del servicio
        let move_service = new ROSLIB.Service({
            ros: data.ros,
            name: '/task_server',
            serviceType: 'nahfy_task_server/Task'
        })

        //definimos el parámetro de la llamada
        let request = new ROSLIB.ServiceRequest({
            type: "NAHFY_DIAGNOSE",
            param_1: 0.00,
            param_2: 0.00,
            param_3: 0.00,
            param_4: 0.00,
            param_5: ""
        })

        move_service.callService(request, (result) => {
            data.service_busy = false
            data.service_response = JSON.stringify(result) // Parse the result
            console.log(data.service_response)


            console.log("Service sent succesfully")

        }, (error) => {
            data.service_busy = false
            console.error(error) // Print error by console
        })	

  }

  function action_transport(){
    data.service_response = ''

        //definimos los datos del servicio
        let move_service = new ROSLIB.Service({
            ros: data.ros,
            name: '/task_server',
            serviceType: 'nahfy_task_server/Task'
        })

        //definimos el parámetro de la llamada
        let request = new ROSLIB.ServiceRequest({
            type: "NAHFY_TRANSPORT",
            param_1: 0.00,
            param_2: 0.00,
            param_3: 0.00,
            param_4: 0.00,
            param_5: ""
        })

        move_service.callService(request, (result) => {
            data.service_busy = false
            data.service_response = JSON.stringify(result) // Parse the result
            console.log(data.service_response)


            console.log("Service sent succesfully")

        }, (error) => {
            data.service_busy = false
            console.error(error) // Print error by console
        })	




  }

  function action_broadcast(){
    data.service_response = ''

    //definimos los datos del servicio
    let move_service = new ROSLIB.Service({
        ros: data.ros,
        name: '/task_server',
        serviceType: 'nahfy_task_server/Task'
    })

    //definimos el parámetro de la llamada
    let request = new ROSLIB.ServiceRequest({
        type: "NAHFY_BROADCAST",
        param_1: 0.00,
        param_2: 0.00,
        param_3: 0.00,
        param_4: 0.00,
        param_5: ""
    })

    move_service.callService(request, (result) => {
        data.service_busy = false
        data.service_response = JSON.stringify(result) // Parse the result
        console.log(data.service_response)


        console.log("Service sent succesfully")

    }, (error) => {
        data.service_busy = false
        console.error(error) // Print error by console
    })	


  }


  function disconnect(){
        data.ros.close()        
        data.connected = false
      console.log('Clic en botón de desconexión')
  }    



})
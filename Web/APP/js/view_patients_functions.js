var dato = [];
var dato2 = [];
var dato3 = [];
var dato4 = [];

var nameChart = [];
var nameChart2 = [];
var nameChart3 = [];
var nameChart4 = [];

function go_admin() {

    window.location = "admin.html";

}


function getPatientAlert() {

    var recurse = servidorAPI + "incidentsPatient/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        var container = document.getElementById("patient-alerts");
        var view = ``;

        // Catch the data and give it a other function

        view = createPatientAlert(data["datos"], view);
        container.innerHTML = view;

    });
}

function getMaps(){
    var idClient = getClientId()

    var recurse = servidorAPI + "maps/?id="+idClient+""; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        fillDropdownMaps(data["datos"]);
    });
}

function fillDropdownMaps(maps){
    var view = ``;
    var container = document.getElementById("dropdown_floors");
    var isFirstMap = true;
    for(var map of maps){
        if(isFirstMap){
            changeMap(map["name"],map["id"]);
            isFirstMap = false;
        }
        view += `<li onclick="changeMap('${map["name"]}','${map["id"]}')"><a href="#">${map["name"]}</a></li>`;
    }
    container.innerHTML = view;
}

function getClientId(){
    var id = get_cookie("nahfy_id");
    return id;
}

function changeMap(mapName, id){
    var container = document.getElementById("dropdown-map-btn");
    var view = ``+mapName+` <span class="caret"></span>`;
    container.innerHTML = view;

    getPatients(id)
}

function getPatients(idMap) {
    var recurse = servidorAPI + "patients/?id="+idMap+""; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        dataCardPatients(data["datos"]);
    });
}

function dataCardPatients(patients) {
    var contenedor = document.getElementById("list-patients");

    var visualizacion = ``;

    for (const paciente of patients) {
        visualizacion += `
            <div class ="col-12 col-md-6 col-xl-4">
            <div class="blackField"></div>
                <div class="card patient-file" onclick="changeItemSelected(this)">
                    <div class="card-header-child" id="topAlert-${paciente["id"]}" style="background-color:white;"></div>
                    <div class="card-body" onclick="getMeasures(${paciente["id"]})">
                        <div id="data-card-patient">
                            <div> <img src="media/images/${paciente["image"]}"> </div>
                            <div>
                            <label><b>${paciente["name"]} ${paciente["surnames"]}</b></label>
                            </div>
                            <div>
                            <label><b>Habitación:</b> ${paciente["room"]}</label>
                            </div>
                        </div>
                        <div id="btn-card-patient">
                            <button class="btn" data-toggle="modal" data-target="#modifyPatient" onclick="edit_view('${paciente["id"]}','${paciente["name"]}','${paciente["surnames"]}','${paciente["nif"]}','${paciente["idRoom"]}','${paciente["image"]}','${paciente["telephon"]}')"><i class="fas fa-id-card"></i></button>
                            <button class="btn" data-toggle="modal" data-target="#erasePatient" onclick="giveId('${paciente["id"]}')"><i class="fas fa-trash"></i></button>
                        </div>
                    </div>
                </div>
            </div>`;
    }
    contenedor.innerHTML = visualizacion;

    getAlertToPatient(patients);
}

function changeItemSelected(item){
    try {
        document.getElementById("itemSelectedCenter").id = "";
    } catch (error) {
    }
    item.id = "itemSelectedCenter";
}

function getAlertToPatient(patients){
    for (const paciente of patients) {
        var recurse = servidorAPI + "incidentsPatient/?id="+paciente["id"]+""; //Recurse to accede the file
        fetch(recurse, {
            method: 'GET'
        }).then(function (response) {
            return response.json(); // return the API response at JSON
        }).then(function (data) {
            changeAlertColorPatient(data["datos"]);
        });
    }
}

function changeAlertColorPatient(patientAlert){
    var val = 0

    if(patientAlert){
        for (const alert of patientAlert) {
            switch(alert["color"]){
                case "red":
                    if(val < 3){
                        val = 3;
                    }
                    break;
                case "orange":
                    if(val < 2){
                        val = 2;
                    }
                    break;
                case "yellow":
                    if(val < 1){
                        val = 1;
                    }
                    break;
                case "green":
                    val = 0
                    break
            }
        }
    }
    
    switch(val){
        case 3:
            val="red";
            break;
        case 2:
            val="orange";
            break;
        case 1:
            val="yellow";
            break;
        case 0:
            val="green";
            break
    }

    if(patientAlert[0]){
        document.getElementById("topAlert-"+patientAlert[0]["idPatient"]).style.backgroundColor = val;
    }
}

async function giveId(id) {
    var container = document.getElementById("deletepatients");
    container.value = id;
}

async function edit_view(id, name, surname, nif, room, image,phone) {
    console.log(image);

    var contain = document.getElementById("input-id");
    var container = document.getElementById("namePatient");
    var container2 = document.getElementById("surnamesPatient");
    var container4 = document.getElementById("nifPatient");
    var container5 = document.getElementById("roomPatient");
    var container6 = document.getElementById("ImagePatient");
    var container7 = document.getElementById("Telephon");

    contain.value = id;
    container.value = name;
    container2.value = surname;
    container4.value = nif;
    container5.value = room;
    container6.value = image;
    container7.value = phone;

}


function search() {
    var input, filter, section, div, p, i;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    section = document.getElementById("list-patients");
    div = section.getElementsByClassName("patient-file")


    for (i = 0; i < div.length; i++) {
        p = div[i].getElementsByTagName("label")[0];
        if (p) {
            var word = filter.split(' ');
            var find = 0;
            for (var filter of word) {
                if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    find++;
                }
            }

            if (find === word.length) {
                div[i].style.display = "";
            } else {
                div[i].style.display = "none";
            }

        }
    }

}




$('.dropdown-menu a').on('click', function () {
    $('.dropdown-toggle').html($(this).html() + '<span class="caret"></span>');
})


function createPatientAlert(alerts, view) {

    for (var alert of alerts) {
        view += `
        <div id="patient-alerts-card-div">
            <div id="patient-alert-color" style="background-color: ${alert["color"]};">
            </div>
            <div id="patient-alerts-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-2 col-lg-2 col-md-6 justify-flex">
                            <img class="image-inherit" src="media/images/${alert["image"]}">
                        </div>
                        <div class="col-10 col-lg-6 col-md-6 justify-flex">
                            <h6><b> ${alert["name"]}</b></h6>
                            <p><b> Problema:</b> ${alert["problem"]}</p>
                            <p style="margin-bottom: revert;"><b> Habitación:</b>  ${alert["room"]}</p>
                        </div>
                        <div class="col-lg-4 col-md-12 justify-flex robots-list-btn">
                            <button class="btn btn-secondary" data-toggle="modal" data-target="#eraseAlert" onclick="giveId2('${alert["id"]}')">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;



    };
    return view

}

function giveId2(id) {
    var container = document.getElementById("deleteIncident");
    container.value = id;
}



function getMeasures(idPatient) {
    var recurse = servidorAPI + "measures/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        console.log(data["datos"]);
        procesarDatosGrafic(data["datos"], idPatient)

    });
}




function procesarDatosGrafic(measures, idPatient) {
    var recurse = servidorAPI + "typemeasure/";
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        console.log(data["datos"]);
        dato = [];
        nameChart = [];
        dato2 = [];
        nameChart2 = [];
        dato3 = [];
        nameChart3 = [];
        dato4 = [];
        nameChart4 = [];

        for (const measure of measures) {

            if (measure["idPatient"] == idPatient) {

                for (const tipo of data["datos"]) {
                    if (measure["idType"] == tipo["id"] && tipo["name"] == "Temperatura") {

                        dato.push(measure["value"]);
                        nameChart.push(measure["moment"]);

                    }
                    if (measure["idType"] == tipo["id"] && tipo["name"] == "pulso") {

                        dato2.push(measure["value"]);
                        nameChart2.push(measure["moment"]);

                    }
                    if (measure["idType"] == tipo["id"] && tipo["name"] == "Sueño") {

                        dato3.push(measure["value"]);
                        nameChart3.push(measure["moment"]);

                    }
                    if (measure["idType"] == tipo["id"] && tipo["name"] == "comida") {

                        dato4.push(measure["value"]);
                        nameChart4.push(measure["moment"]);

                    }
                }

            }



        }
        console.log(dato);
        console.log(nameChart);

        var popCanvas = document.getElementById("temp_chart");
        var popCanvas2 = document.getElementById("beat_chart");
        var popCanvas3 = document.getElementById("sleep_chart");
        var popCanvas4 = document.getElementById("calory_chart");

        popCanvas.style.display = "block";
        popCanvas2.style.display = "block";
        popCanvas3.style.display = "block";
        popCanvas4.style.display = "block";

        var textCanvas = document.getElementById("temp_chart_text");
        var textCanvas2 = document.getElementById("beat_chart_text");
        var textCanvas3 = document.getElementById("sleep_chart_text");
        var textCanvas4 = document.getElementById("calory_chart_text");

        textCanvas.style.display = "none";
        textCanvas2.style.display = "none";
        textCanvas3.style.display = "none";
        textCanvas4.style.display = "none";

        var barChart = new Chart(popCanvas, {
            type: 'line',
            data: {
                labels: nameChart,
                datasets: [{
                    labelString: 'Grados',
                    data: dato,
                    backgroundColor: [
                        'rgba(153, 102, 255, 0.6)'
                    ]
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });

        var barChart2 = new Chart(popCanvas2, {
            type: 'line',
            data: {
                labels: nameChart2,
                datasets: [{
                    labelString: 'Grados',
                    data: dato2,
                    backgroundColor: [
                        'rgba(153, 102, 255, 0.6)'
                    ]
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });

        var barChart3 = new Chart(popCanvas3, {
            type: 'line',
            data: {
                labels: nameChart3,
                datasets: [{
                    labelString: 'Grados',
                    data: dato3,
                    backgroundColor: [
                        'rgba(153, 102, 255, 0.6)'
                    ]
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });

        var barChart4 = new Chart(popCanvas4, {
            type: 'line',
            data: {
                labels: nameChart4,
                datasets: [{
                    labelString: 'Grados',
                    data: dato4,
                    backgroundColor: [
                        'rgba(153, 102, 255, 0.6)'
                    ]
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });


    });

}

function select(){
    console.log("llegar")
    var checkList = document.getElementById('check-list');
    var checkBoxList = checkList.getElementsByTagName("input");
    console.log(checkBoxList);
    for (var i = checkBoxList.length-1; i > 0; i--) {
        checkBoxList[i].addEventListener('change', function() {
            console.log("llegar dentro")
            console.log(checkBoxList[i])

            checkBoxList[i].checked = 1;
            
        });
        
    }

}

function dele(){
    var checkList = document.getElementById('check-list');
    var checkBoxList = checkList.getElementsByTagName("input");
    var checkBoxSelectedItems = new Array();
    var checkList2 = document.getElementById('check-list2');
    var checkList3 = document.getElementById('check-list3');
    var checkList4 = document.getElementById('check-list4');
  
    var checkBoxList2 = checkList2.getElementsByTagName("input");
    var checkBoxList3 = checkList3.getElementsByTagName("input");
    var checkBoxList4 = checkList4.getElementsByTagName("input");
  
    var checkBoxSelectedItems2 = new Array();
    var checkBoxSelectedItems3 = new Array();
    var checkBoxSelectedItems4 = new Array();


    for (var i = 0; i < checkBoxList.length; i++) {
        if (checkBoxList[i].checked) {

            checkBoxSelectedItems.push(checkBoxList[i].value);
            console.log(checkBoxList[i].value);
        }
        
    }

    for (var a = 0; a < checkBoxList2.length; a++) {
        if (checkBoxList2[a].checked) {

            checkBoxSelectedItems2.push(checkBoxList2[a].value);
            console.log(checkBoxList2[a].value);
        }
    }

    for (var b = 0; b < checkBoxList3.length; b++) {
        if (checkBoxList3[b].checked) {

            checkBoxSelectedItems3.push(checkBoxList3[b].value);
            console.log(checkBoxList3[b].value);
        }
    }

    for (var c = 0; c < checkBoxList4.length; c++) {
        if (checkBoxList4[c].checked) {

            checkBoxSelectedItems4.push(checkBoxList4[c].value);
            console.log(checkBoxList4[c].value);
        }
    }

    var container = document.getElementById("deleteMeasures");
    container.value = checkBoxSelectedItems;
    var container2 = document.getElementById("deleteMeasures2");
    var container3 = document.getElementById("deleteMeasures3");
    var container4 = document.getElementById("deleteMeasures4");
   
    container2.value = checkBoxSelectedItems2;
    container3.value = checkBoxSelectedItems3;
    container4.value = checkBoxSelectedItems4;

}



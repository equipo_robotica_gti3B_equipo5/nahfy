// -------------------------------------------------
// MENU BAR
// -------------------------------------------------
$(function() {
    var header = $(".start-style");
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
        if (scroll >= 10) {
            header.removeClass('start-style').addClass("scroll-on");
            document.getElementById("up-button").style.display = "block";
        } else {
            header.removeClass("scroll-on").addClass('start-style');
            document.getElementById("up-button").style.display = "none";
        }
    });
});

function scroll_web_up() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

// -------------------------------------------------
// SESSION CLOSE
// -------------------------------------------------
function close_session(){
    if(get_cookie("nahfy_user").length >= 1){
        delete_cookie("nahfy_user");
        delete_cookie("nahfy_password");
        delete_cookie("nahfy_rol");
        delete_cookie("id");

        go_login();
    }
}

function go_login(){
    window.location.href="login.html";
}
  
// -------------------------------------------------
// COOKIES
// -------------------------------------------------
function set_cookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function get_cookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function delete_cookie(name) {
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

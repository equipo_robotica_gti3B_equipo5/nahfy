function getCustomers() {

    var recurse = servidorAPI + "users/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {

        // Catch the data and give it a other function
        creatCustomersCard(data["datos"])
        selectionId()


    });
}

function creatCustomersCard(customers) {
    console.log(customers)
    var container = document.getElementById("list_customers")



    var view = ``;
    for (const customer of customers) {
        console.log(customer)
        view += `
            <div class ="col-12">
            <div class="blackField"></div>
                <div class="card customer-file" onclick="changeItemSelected(this)">
                    <div class="card-header-child child-content" style="background-color:white;">
                        <p class="client-name">${customer["name"]}</p>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <button class="dropdown-item" onclick="edit_view('${customer["email"]}','${customer["name"]}','${customer["password"]}','${customer["id"]}','${customer["image"]}')"
                                data-toggle="modal" data-target="#exampleModalCentered"><i class="fas fa-edit"></i></button>
                                <button class="dropdown-item" onclick="giveId('${customer["id"]}')" data-toggle="modal" data-target="#exampleModal3"><i class="fas fa-trash-alt"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" onclick="getCenters('${customer["id"]}','${customer["email"]}')">
                        <div id="data-card-customer">
                            <div> <img src="media/images/${customer["image"]}"> </div>
                            <div>
                            <label><b>Email:</b> ${customer["email"]}</label>
                            </div>
                        </div>
                        <div id="btn-card-customer">
                            <button class="btn" onclick="edit_view('${customer["email"]}','${customer["name"]}','${customer["password"]}','${customer["id"]}')" data-toggle="modal" data-target="#exampleModalCentered6">
                                <i class="fas fa-envelope"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>`;

    }



    container.innerHTML = view;
}

function changeItemSelected(item){
    try {
        document.getElementById("itemSelectedCenter").id = "";
    } catch (error) {
    }
    item.id = "itemSelectedCenter";
}

async function edit_view(email, name, password, id, image) {
    var container = document.getElementById("clientMail");
    var container2 = document.getElementById("clientName");
    var container3 = document.getElementById("ClientImage");

    var container4 = document.getElementById("clientPassword");
    var container6 = document.getElementById("clientId");

    var cont1 = document.getElementById("input-email");
    var cont2 = document.getElementById("input-name");


    container.value = email;
    container2.value = name;
    container3.value = image;

    container4.value = password;
    container6.value = id;

    cont1.value = email;
    cont2.value = name;

}

async function giveId(id) {
    var container = document.getElementById("deleteclient");
    container.value = id;

}

function search() {
    var input, filter, section, div, p, i;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    section = document.getElementById("list_customers");
    div = section.getElementsByClassName("card-information")


    for (i = 0; i < div.length; i++) {
        p = div[i].getElementsByTagName("p")[0];
        if (p) {
            var word = filter.split(' ');
            var find = 0;
            for (var filter of word) {
                if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    find++;
                }
            }

            if (find === word.length) {
                div[i].style.display = "";
            } else {
                div[i].style.display = "none";
            }

        }
    }

}


function getCenters(idClient, email) {
    var recurse = servidorAPI + "centers/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        // Catch the data and give it a other function
        createCenters(idClient, email, data["datos"]);

    });
}


function createCenters(idClient, email, centers) {
    console.log(email);
    var info = document.getElementById("card-center-info");
    info.style.display = "none"
    var container = document.getElementById("list_centers");

    var view = ``;
    


    var recurse = servidorAPI + "customerscenters/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        for (const relation of data["datos"]) {
            for (const center of centers) {

                if (relation["idCustomer"] == idClient && relation["idCenter"] == center["idCenter"]) {
                    console.log(center)
                    console.log(email)
                    console.log(relation)

                    view += `
                        <div class ="col-12 col-md-6">
                        <div class="blackField"></div>
                            <div class="card center-section-file">
                                <div class="card-header-child child-content" style="background-color:white;">
                                    <p class="client-name">${center["name"]}</p>
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <button class="dropdown-item" onclick="edit_view2('${center["idCenter"]}','${center["name"]}','${center["direction"]}','${center["healthArea"]}','email', '${center["image"]}')" data-toggle="modal" data-target="#exampleModalCentered5"><i class="fas fa-edit"></i></button>
                                            <button class="dropdown-item" onclick="giveId3('${center["idCenter"]}')" data-toggle="modal" data-target="#exampleModal5"><i class="fas fa-trash-alt"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div id="data-card-customer">
                                        <div> <img src="media/images/${center["image"]}"> </div>
                                    </div>
                                    <div id="btn-card-customer">
                                        <button class="btn" onclick="edit_view2('${center["idCenter"]}','${center["name"]}','${center["direction"]}','${center["healthArea"]}', '${email}','image')" data-toggle="modal" data-target="#exampleModalCentered7">
                                            <i class="fas fa-envelope"></i>
                                        </button>
                                        <div class="icon-information col-2 btn">
                                            <a href="patients.html?idCenter=${center["idCenter"]}" class="btn">
                                                <i class="far fa-id-card" ></i>
                                            </a>
                                        </div>
                                        <div class="icon-information col-2 btn">
                                            <a href="maps_robot.html?idCenter=${center["idCenter"]}" class="btn">
                                                <i class="fas fa-map" ></i>
                                            </a>
                                        </div>
                                        <div class="icon-information col-2 btn">
                                            <a href="maps_robot.html?idCenter=${center["idCenter"]}" class="btn">
                                                <i class="fas fa-robot" ></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;

                }

            }
           
            container.innerHTML = view;
            console.log(email);



        }


    });



}

function selectionId() {
    var container = document.getElementById("selectorID");
    var view = ``;

    var recurse = servidorAPI + "selectID/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        for (const id of data["datos"]) {
            view += `<option >'${id["id"]}'</option>`;

        }
        container.innerHTML = view;

    });

    var container2 = document.getElementById("selectorIDCenter");
    var view2 = ``;

    var recurse = servidorAPI + "centers/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        for (const id of data["datos"]) {
            view2 += `<option>'${id["idCenter"]}'</option>`;

        }
        container2.innerHTML = view2;

    });

}

function search2() {
    var input, filter, section, div, p, i;
    input = document.getElementById("search2");
    filter = input.value.toUpperCase();
    section = document.getElementById("list_centers");
    div = section.getElementsByClassName("card-information")


    for (i = 0; i < div.length; i++) {
        p = div[i].getElementsByTagName("p")[0];
        if (p) {
            var word = filter.split(' ');
            var find = 0;
            for (var filter of word) {
                if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    find++;
                }
            }

            if (find === word.length) {
                div[i].style.display = "";
            } else {
                div[i].style.display = "none";
            }

        }
    }

}

function edit_view2(id, name, direction, area, email, image) {
    console.log(email);
    var container = document.getElementById("centerId");
    var container2 = document.getElementById("centerName");
    var container3 = document.getElementById("direction");
    var container4 = document.getElementById("healthArea");
    var container5 = document.getElementById("imageCenterEdit");


    var cont1 = document.getElementById("input-email2");
    var cont2 = document.getElementById("input-name2");

    container.value = id;
    container2.value = name;
    container3.value = direction;
    container4.value = area;
    container5.value = image;


    cont1.value = email;
    cont2.value = name;
    console.log(email);


}

async function giveId3(id) {
    var container = document.getElementById("deletecenter");
    container.value = id;

}

function getAlert() {

    var recurse = servidorAPI + "incidents/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        // Catch the data and give it a other function
        createAlert(data["datos"]);

    });
}


function createAlert(alerts) {
    var container = document.getElementById("list_robots");

    var view = ``;
    for (var alert of alerts) {
        var clase = "";

        switch(alert["problem"]){
            case low_energy:
                clase = "fa-battery-quarter";
                break;
            case not_energy:
                clase = "fa-battery-empty";
            break;
            case not_enable:
                clase = "fa-ban";
                break;
        }

        view += `
        <div id="patient-alerts-card-div">
            <div id="patient-alerts-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-2 col-lg-2 col-md-6 justify-flex image-problem-robot">
                            <i class="fas ${clase}"></i>
                        </div>
                        <div class="col-10 col-lg-6 col-md-6 justify-flex">
                            <h6><b> ${alert["name"]}</b></h6>
                            <p><b> Problema:</b> ${alert["problem"]}</p>
                            <p><b> Planta:</b> ${alert["nameMap"]}</p>
                            <p style="margin-bottom: revert;"><b> Cliente y centro:</b>  ${alert["email"]}, ${alert["healthArea"]}</p>
                        </div>
                        <div class="col-lg-4 col-md-12 justify-flex robots-list-btn">
                        <button class="btn btn-primary" onclick="contact('${alert["name"]}','${alert["email"]}','${alert["healthArea"]}')" data-toggle="modal" data-target="#exampleModalCentered8">Contactar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
    };
    container.innerHTML = view;

}



function getPatientAlert() {

    var recurse = servidorAPI + "incidentsPatient/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {


        // Catch the data and give it a other function

        createPatientAlert(data["datos"]);


    });
}

function createPatientAlert(alerts) {
    var container = document.getElementById("patient-alerts");
    var view = ``;
    
    for (var alert of alerts) {
        console.log(alert);
        view += `
        <div id="patient-alerts-card-div">
            <div id="patient-alert-color" style="background-color: ${alert["color"]};">
            </div>
            <div id="patient-alerts-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-2 col-lg-2 col-md-6 justify-flex">
                            <img class="image-inherit" src="media/images/${alert["image"]}">
                        </div>
                        <div class="col-10 col-lg-6 col-md-6 justify-flex">
                            <h6><b> ${alert["name"]}</b></h6>
                            <p><b> Problema:</b> ${alert["problem"]}</p>
                            <p style="margin-bottom: revert;"><b> Habitación:</b>  ${alert["room"]}</p>
                        </div>
                        <div class="col-lg-4 col-md-12 justify-flex robots-list-btn">
                        <button class="btn btn-primary" onclick="contact('${alert["name"]}','${alert["email"]}','${alert["healthArea"]}')" data-toggle="modal" data-target="#exampleModalCentered8">Contactar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
    };
    container.innerHTML = view;


}

function contact(name, email, area) {
    var container = document.getElementById("input-name3");
    var container2 = document.getElementById("input-email3");
    var container3 = document.getElementById("input-area");

    container.value = name;
    container2.value = email;
    container3.value = area;
}


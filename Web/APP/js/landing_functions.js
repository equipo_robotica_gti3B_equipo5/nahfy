$(function() {
    name_class = "nav-item pl-4 pl-md-0 ml-0 ml-md-4";
    id_list = ["nav-welcome","nav-information","nav-images","nav-contact"]
    selected_id = "nav-welcome"
    $(window).scroll(function() {
        if (window.innerWidth <= 800){
            value_nav_welcome = 800
            value_nav_information = 3500
            value_nav_images = 4200
            
        }else if(window.innerWidth > 800 && window.innerWidth <= 1200){
            value_nav_welcome = 800
            value_nav_information = 2000
            value_nav_images = 2500
        }
        else{
            value_nav_welcome = 800
            value_nav_information = 1000
            value_nav_images = 2000
        }    
        var scroll = $(window).scrollTop();
        if (scroll < value_nav_welcome){
            selected_id = "nav-welcome"
        }
        else if (scroll >= value_nav_welcome && scroll < value_nav_information) {
            selected_id = "nav-information"
        }
        else if (scroll >= value_nav_information && scroll < value_nav_images){
            selected_id = "nav-images"
        }else{
            selected_id = "nav-contact"
        }

        document.getElementById(selected_id).className = name_class + " selected-item"
        for (i = 0; i < id_list.length; i++) {
            if (id_list[i] != selected_id){
                document.getElementById(id_list[i]).className = name_class
            }
        }
    });
});

function detectCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0 && (name.length != c.length))  {
			return true;
		}
	}
	return false;
}

function removeCookie(cname){
    setCookie(cname,"",-1);
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();

	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();

	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function existenCookies(){
	if(detectCookie("exitoContacto")==true){
		//mostramos el mensaje
		document.getElementById("exit-sent-mail").style.display="block"
		//borramos las cookies
		removeCookie("exitoContacto")
	}
	if(detectCookie("errorEnvio")==true){
		//mostramos el mensaje
		document.getElementById("error-sent-mail").style.display="block"
		//borramos las cookies
		removeCookie("errorEnvio")
	}
}
var last_movement_input_checked = "coordinates"
var coordinates_x = new Map(); var coordinates_y = new Map();

/*-----------------------------------------------------------------------------------
---------------------------------ROBOTS----------------------------------------------
-----------------------------------------------------------------------------------*/

function getRobots(idMap){

    var recurse = servidorAPI + "robots/?idMap="+idMap+""; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        getRobotsPatients(data);

    });
}

function getRobotsPatients(dataRobots){

    var recurse = servidorAPI + "incidents/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (dataAlerts) {
        createRobotList(dataRobots["datos"],dataAlerts["datos"]);

    });
}

function createRobotList(robots,incidents){
    var container = document.getElementById("robots-list");
    var view = ``;
    for(var robot of robots){
        view += `
        <div id="robots-card-div">
            <div id="robots-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-2 justify-flex">
                            <img class="image-inherit" src="media/images/${robot["image"]}">
                        </div>
                        <div class="col-10 col-lg-7 col-md-10 justify-flex">
                            <h6><b> ${robot["name"]}</b></h6>
                            <p><b>Estado actual: </b>${robot["state"]}</p>
                        </div>
                        <div class="col-lg-3 col-md-12 justify-flex robots-list-btn">
                        <div class="multiple-btn">
                            <button class="btn btn-secondary" onclick="giveId('${robot["id"]}','${getClientCenter()}')" data-toggle="modal" data-target="#exampleModal3">Eliminar</button>
                            <button class="btn btn-primary " onclick="edit_view('${robot["id"]}','${robot["name"]}','${robot["idCenter"]}','${robot["idMap"]}','${robot["state"]}','${robot["image"]}','${getClientCenter()}')" data-toggle="modal" data-target="#exampleModalCentered" >Editar</button>
                        </div>
                            <div id="incidents-robot-${robot["id"]}" class="container-alerts-robots-icon"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
    };

    container.innerHTML = view;
    
    var incidentContainer = document.getElementById("incidents-robot-"+robot["id"]);

    for(var robot of robots){
        for(var incident of incidents){
            if(robot["id"] == incident["idRobot"]){
                switch(incident["problem"]){
                    case low_energy:
                        incidentContainer.innerHTML += `<i class="fas fa-battery-quarter"></i>`;
                        break;
                    case not_energy:
                        incidentContainer.innerHTML += `<i class="fas fa-battery-empty"></i>`;
                    break;
                    case not_enable:
                        incidentContainer.innerHTML += `<i class="fas fa-ban"></i>`;
                        break;
                }
            }
        }
    }
}

function edit_view(id,name, idCenter,idMap, state, image, center){
    console.log(idCenter);
    var container = document.getElementById("centerId");
    var container2 = document.getElementById("nameId");
    var container3 = document.getElementById("mapId");
    var container4 = document.getElementById("editstate");
    var container5 = document.getElementById("editImageRobot");
    var container6 = document.getElementById("robotId");
    var container7 = document.getElementById("idCenter");

    container.value = idCenter;
    container2.value = name;
    container3.value = idMap;
    container4.value = state;
    container5.value = image;
    container6.value = id;
    container7.value = center;

}

async function giveId(id, center){
    var container = document.getElementById("deleterobot");
    container.value = id;

    var container = document.getElementById("idCenter2");
    container.value = center;
    
}

function putId(){
    var container = document.getElementById("idCenter3");
    container.value = getClientCenter();
}

$('.dropdown-menu a').on('click', function () {
    $('.dropdown-toggle').html($(this).html() + '<span class="caret"></span>');
})

/*-----------------------------------------------------------------------------------
---------------------------------MAPS----------------------------------------------
-----------------------------------------------------------------------------------*/

function getMaps(){
    var idCenter = getClientCenter()

    var recurse = servidorAPI + "maps/?idCenter="+idCenter+""; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        fillDropdownMaps(data["datos"]);
    });
}

function showFirstMap(map){
    changeMap(map["name"],map["plan"],map["id"]);
    getRobots(map["id"]);
}

function fillDropdownMaps(maps){
    var view = ``;
    var container = document.getElementById("dropdown_floors");
    var isFirtsMap = true;
    for(var map of maps){
        if(isFirtsMap){
            showFirstMap(map);
            isFirtsMap = false;
        }
        view += `<li onclick="changeMap('${map["name"]}','${map["plan"]}','${map["id"]}')"><a href="#">${map["name"]}</a></li>`;
    }
    container.innerHTML = view;
}

function changeMap(mapName, mapImage, id){
    var container = document.getElementById("map-image-container");
    var view = `<img alt="mapa" id="map-image" src="./media/images/`+mapImage+`">`;
    container.innerHTML = view;

    var container = document.getElementById("dropdown-map-btn");
    var view = ``+mapName+` <span class="caret"></span>`;
    container.innerHTML = view;
    getRobots(id);
}

/*-----------------------------------------------------------------------------------
-----------------------------PATIENT ALERTS------------------------------------------
-----------------------------------------------------------------------------------*/

function getPatientAlert(){

    var recurse = servidorAPI + "incidentsPatient/"; //Recurse to accede the file
    fetch(recurse, {
        method: 'GET'
    }).then(function (response) {
        return response.json(); // return the API response at JSON
    }).then(function (data) {
        var container = document.getElementById("patient-alerts");
        var view = ``;

        // Catch the data and give it a other function
        
        view = createPatientAlert(data["datos"], view);
        container.innerHTML = view;

    });
}

function createPatientAlert(alerts, view){
    
    for(var alert of alerts){
        view += `
        <div id="patient-alerts-card-div">
            <div id="patient-alert-color" style="background-color: ${alert["color"]};">
            </div>
            <div id="patient-alerts-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-2 col-lg-2 col-md-6 justify-flex">
                            <img class="image-inherit" src="media/images/${alert["image"]}">
                        </div>
                        <div class="col-10 col-lg-6 col-md-6 justify-flex">
                            <h6><b> ${alert["name"]}</b></h6>
                            <p><b> Problema:</b> ${alert["problem"]}</p>
                            <p style="margin-bottom: revert;"><b> Habitación:</b>  ${alert["room"]}</p>
                        </div>
                        <div class="col-lg-4 col-md-12 justify-flex robots-list-btn">
                            <button class="btn btn-secondary" data-toggle="modal" data-target="#eraseAlert" onclick="giveId2('${alert["id"]}','${getClientCenter()}')">Eliminar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
      
    };
    return view
   
}

async function giveId2(id, center) {
    var container = document.getElementById("deleteIncident");
    container.value = id;

    var container = document.getElementById("idCenter4");
    container.value = center;
}

/*-----------------------------------------------------------------------------------
-----------------------------------OTHERS--------------------------------------------
-----------------------------------------------------------------------------------*/

function getClientCenter(){
    var url = new URLSearchParams(window.location.search);
    var param = url.get('idCenter'); 
    return param;
}

$('.dropdown-menu a').on('click', function(){    
    $('.dropdown-toggle').html($(this).html() + '<span class="caret"></span>');    
})

function checkBtnCanEnable(name1, name2, btnId){
    var checkbox1 = document.getElementsByName(name1);
    var checkbox2 = document.getElementsByName(name2);

    var count1 = 0;
    var count2 = 0;

    for(var i=0, n=checkbox1.length;i<n;i++) {
        if(checkbox1[i].checked){
            count1++;
        }
    }

    for(var i=0, n=checkbox2.length;i<n;i++) {
        if(checkbox2[i].checked){
            count2++;
        }
    }

    var btn = document.getElementById(btnId);

    if(btnId == "btn-transport"){
        if(count1 > 0 || count2 > 0){
            btn.disabled = false;
        }else{
            btn.disabled = true;
        }
    }else{
        if(count1 > 0 && count2 > 0){
            btn.disabled = false;
        }else{
            btn.disabled = true;
        }
    }

}


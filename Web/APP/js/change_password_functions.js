function check_new_password(){
    if(document.getElementById("password").value == "" || document.getElementById("password2").value == ""){
        document.getElementById("error-empty-password").style.display = "block";
        document.getElementById("error-different-password").style.display = "none";
    }
    else if(document.getElementById("password").value != document.getElementById("password2").value){
        document.getElementById("error-different-password").style.display = "block";
        document.getElementById("error-empty-password").style.display = "none";

    }else{
        document.getElementById("error-different-password").style.display = "none";
        document.getElementById("error-empty-password").style.display = "none";
        // TODO: RELLENAR
    }
}
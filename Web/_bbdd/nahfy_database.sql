-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 17-06-2021 a las 22:38:29
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nahfy_database`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centers`
--

CREATE TABLE `centers` (
  `idCenter` int(11) NOT NULL,
  `name` text COLLATE utf32_spanish_ci NOT NULL,
  `direction` text COLLATE utf32_spanish_ci NOT NULL,
  `healthArea` text COLLATE utf32_spanish_ci NOT NULL,
  `image` text COLLATE utf32_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish_ci;

--
-- Volcado de datos para la tabla `centers`
--

INSERT INTO `centers` (`idCenter`, `name`, `direction`, `healthArea`, `image`) VALUES
(2, 'Centro Salud Grao de Gandia', 'Carrer de la Goleta', 'Gandia', 'centro1.jpg'),
(8, 'has', 'bcsudhncki 3', 'jejehahha', 'centro1.jpg'),
(10, 'hospital', 'bcsudhncki 3', 'Gandia', 'centro1.jpg'),
(11, 'ddd', 'www', 'djnjd', 'centro1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customerscenters`
--

CREATE TABLE `customerscenters` (
  `idCustomerCenter` int(11) NOT NULL,
  `idCustomer` int(11) NOT NULL,
  `idCenter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `customerscenters`
--

INSERT INTO `customerscenters` (`idCustomerCenter`, `idCustomer`, `idCenter`) VALUES
(1, 12, 2),
(2, 11, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidents`
--

CREATE TABLE `incidents` (
  `id` int(11) NOT NULL,
  `problem` text COLLATE utf32_spanish_ci NOT NULL,
  `description` text COLLATE utf32_spanish_ci NOT NULL,
  `type` text COLLATE utf32_spanish_ci NOT NULL,
  `idRobot` int(11) NOT NULL,
  `idPatient` int(11) NOT NULL,
  `isAlert` tinyint(1) NOT NULL,
  `moment` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `color` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish_ci;

--
-- Volcado de datos para la tabla `incidents`
--

INSERT INTO `incidents` (`id`, `problem`, `description`, `type`, `idRobot`, `idPatient`, `isAlert`, `moment`, `color`) VALUES
(3, 'Bateria baja', 'www', 'robot', 1, 1, 1, '2021-05-13 16:04:22', 'orange'),
(5, 'falta medir pulso', 'www', 'patient', 1, 1, 0, '2021-04-25 14:29:53', 'yellow'),
(6, 'fiebre', 'www', 'patient', 1, 1, 1, '2021-04-25 14:29:58', 'orange'),
(7, 'Pulso acelerado', 'Medir pulso', 'patient', 1, 2, 1, '2021-05-13 18:37:03', 'green');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `maps`
--

CREATE TABLE `maps` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf32_spanish_ci NOT NULL,
  `idCenter` int(11) NOT NULL,
  `plan` text COLLATE utf32_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish_ci;

--
-- Volcado de datos para la tabla `maps`
--

INSERT INTO `maps` (`id`, `name`, `idCenter`, `plan`) VALUES
(1, 'mapaGandia', 2, 'map.jpg'),
(2, 'rrrrrobto', 11, 'maps_Helloa.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `measures`
--

CREATE TABLE `measures` (
  `id` int(11) NOT NULL,
  `idType` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `idPatient` int(11) NOT NULL,
  `moment` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish_ci;

--
-- Volcado de datos para la tabla `measures`
--

INSERT INTO `measures` (`id`, `idType`, `value`, `idPatient`, `moment`) VALUES
(2, 2, 40, 1, '2021-04-25 22:00:00'),
(3, 3, 90, 1, '2021-04-25 22:00:00'),
(6, 4, 8, 1, '2021-04-26 22:00:00'),
(7, 5, 60, 1, '2021-04-26 22:00:00'),
(15, 3, 120, 1, '2021-04-27 22:00:00'),
(17, 5, 120, 1, '2021-04-27 22:00:00'),
(18, 1, 37, 2, '2021-05-17 11:21:13'),
(19, 3, 120, 2, '2021-04-27 22:00:00'),
(20, 2, 28, 10, '2021-05-19 15:22:15'),
(21, 2, 29, 10, '2021-05-19 15:22:15'),
(22, 2, 40, 10, '2021-04-25 22:00:00'),
(23, 3, 90, 10, '2021-04-25 22:00:00'),
(24, 4, 8, 10, '2021-04-26 22:00:00'),
(25, 5, 60, 10, '2021-04-26 22:00:00'),
(26, 3, 120, 10, '2021-04-27 22:00:00'),
(27, 5, 120, 10, '2021-04-27 22:00:00'),
(28, 3, 120, 10, '2021-04-27 22:00:00'),
(29, 2, 29, 10, '2021-05-19 15:22:15'),
(30, 2, 27, 10, '2021-05-19 15:22:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patients`
--

CREATE TABLE `patients` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf32_spanish_ci NOT NULL,
  `surnames` text COLLATE utf32_spanish_ci NOT NULL,
  `nif` text COLLATE utf32_spanish_ci NOT NULL,
  `idRoom` int(50) NOT NULL,
  `image` text COLLATE utf32_spanish_ci NOT NULL,
  `telephon` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish_ci;

--
-- Volcado de datos para la tabla `patients`
--

INSERT INTO `patients` (`id`, `name`, `surnames`, `nif`, `idRoom`, `image`, `telephon`) VALUES
(1, 'Juan', 'Antonio', '13572468Z', 1, 'paciente1.png', 0),
(2, 'Pepe', 'U', '12356g', 1, 'paciente2.png', 0),
(6, 'has', 'qqfff', '12356g', 2, 'paciente1.png', 0),
(7, 'Sara', 'Melero', '12356g', 1, 'paciente2.png', 0),
(8, 'Pepe', 'Garcia', '13572468Z', 1, 'paciente1.png', 0),
(9, 'Pepita', 'Antonia', '1984156g', 2, 'paciente2.png', 0),
(10, 'Juana', 'Carreño', '12865356g', 11, 'paciente2.png', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `robots`
--

CREATE TABLE `robots` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf32_spanish_ci NOT NULL,
  `idCenter` int(11) NOT NULL,
  `idMap` int(11) NOT NULL,
  `x_begin` float NOT NULL,
  `y_begin` float NOT NULL,
  `state` text COLLATE utf32_spanish_ci NOT NULL,
  `image` text COLLATE utf32_spanish_ci NOT NULL,
  `x_now` float NOT NULL,
  `y_now` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish_ci;

--
-- Volcado de datos para la tabla `robots`
--

INSERT INTO `robots` (`id`, `name`, `idCenter`, `idMap`, `x_begin`, `y_begin`, `state`, `image`, `x_now`, `y_now`) VALUES
(1, 'Robot-Asistente', 2, 1, 0, 0, 'Esperando', 'robot.jpeg', 0, 0),
(2, 'robito', 5, 2, 1, 1, 'Buscando paciente', 'robot.jpeg', 0, 0),
(8, 'rrrff', 12, 1, 1, 1, 'Esperando', 'robot2.jpeg', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room` text COLLATE utf32_spanish_ci NOT NULL,
  `idMap` int(11) NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish_ci;

--
-- Volcado de datos para la tabla `rooms`
--

INSERT INTO `rooms` (`id`, `room`, `idMap`, `x`, `y`) VALUES
(1, 'Almacén (punto partida)', 1, 0, 0),
(3, 'Recepción', 1, 0.635, 6.16),
(4, 'Pasillo', 1, -3.42, 2.45),
(5, 'Habitación 1', 1, -8.29, -1.64),
(6, 'Baño 1', 1, -5.79, -1.2),
(7, 'Habitación 2', 1, -6.77, 0.93),
(8, 'Baño 2', 1, -5.19, 3.32),
(9, 'Habitación 3', 1, -7.39, 6.87),
(10, 'Baño 3', 1, -5.22, 6.82),
(11, 'Habitación 342', 2, -5.22, 6.82);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `typemeasure`
--

CREATE TABLE `typemeasure` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf32_spanish_ci NOT NULL,
  `units` text COLLATE utf32_spanish_ci NOT NULL,
  `optimalValue` int(11) NOT NULL,
  `alertValue` int(11) NOT NULL,
  `criticalValue` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish_ci;

--
-- Volcado de datos para la tabla `typemeasure`
--

INSERT INTO `typemeasure` (`id`, `name`, `units`, `optimalValue`, `alertValue`, `criticalValue`) VALUES
(1, 'Ozono', 'mg/m^3', 0, 0, 0),
(2, 'Temperatura', 'ºC', 36, 38, 40),
(3, 'pulso', 'pulsos', 90, 120, 200),
(4, 'Sueño', 'horas', 8, 5, 2),
(5, 'comida', 'calorias', 100, 50, 30),
(6, 'pasos diarios', 'pasos', 0, 0, 0),
(7, 'distancia', 'm', 0, 0, 0),
(8, 'bateria', 'porcentaje', 100, 25, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf32_spanish_ci NOT NULL,
  `email` text COLLATE utf32_spanish_ci NOT NULL,
  `password` text COLLATE utf32_spanish_ci NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `image` text COLLATE utf32_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `isAdmin`, `image`) VALUES
(1, 'NAHFY', 'nahfyproyecto@gmail.com', '1234', 1, 'paciente1.png'),
(11, '2111', '1234@gmail.com', '12', 0, 'paciente1.png'),
(12, 'Señor', 'cliente1@gmail.com', '1234', 0, 'paciente1.png'),
(13, 'hola', '123@gmail.com', '1234', 0, 'paciente1.png');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `centers`
--
ALTER TABLE `centers`
  ADD PRIMARY KEY (`idCenter`);

--
-- Indices de la tabla `customerscenters`
--
ALTER TABLE `customerscenters`
  ADD PRIMARY KEY (`idCustomerCenter`);

--
-- Indices de la tabla `incidents`
--
ALTER TABLE `incidents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idRobot` (`idRobot`),
  ADD KEY `idPaciente` (`idPatient`);

--
-- Indices de la tabla `maps`
--
ALTER TABLE `maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCentro` (`idCenter`);

--
-- Indices de la tabla `measures`
--
ALTER TABLE `measures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idTipo` (`idType`),
  ADD KEY `idPaciente` (`idPatient`);

--
-- Indices de la tabla `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `robots`
--
ALTER TABLE `robots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCentro` (`idCenter`),
  ADD KEY `idMapa` (`idMap`);

--
-- Indices de la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idMapa` (`idMap`);

--
-- Indices de la tabla `typemeasure`
--
ALTER TABLE `typemeasure`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `centers`
--
ALTER TABLE `centers`
  MODIFY `idCenter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `customerscenters`
--
ALTER TABLE `customerscenters`
  MODIFY `idCustomerCenter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `incidents`
--
ALTER TABLE `incidents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `maps`
--
ALTER TABLE `maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `measures`
--
ALTER TABLE `measures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `robots`
--
ALTER TABLE `robots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `typemeasure`
--
ALTER TABLE `typemeasure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

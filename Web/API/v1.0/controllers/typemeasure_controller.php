<?php
/**
 * Controller of the non-Associated resource, links the model corresponding to the method of the request made 
 * and the general view of the API
 * @version 1.2
 */


// The resource model is included for the method of the request made (GET, POST, PUT or DELETE)
require('models/typemeasure/typemeasure_model_'.$method.'.php');

// Resource view included
require('views/principal_view.php');
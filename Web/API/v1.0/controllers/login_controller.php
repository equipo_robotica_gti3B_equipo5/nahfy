<?php
/**
 * Controlador del recurso inicioSesion, enlaza el modelo correspondiente al metodo de la peticion realizada
 * y la vista general de la API
 */


// The resource model is included for the method of the request made (GET, POST, PUT or DELETE)
require('models/login/login_model_'.$method.'.php');

// Resource view included
require('views/principal_view.php');
<?php

/**
 * General response view to API requests
 * All sources are allowed for the API, GET, POST, PUT and DELETE methods
 * and the output is processed in JSON format
 * @version 1.1
 */


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header("Content-Type: application/json; charset=utf-8");
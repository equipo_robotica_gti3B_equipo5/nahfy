<?php

/**
 * View for login via web, which creates user session variables
 */


 // If the user exits
 if(count($response)>0){
    $_SESSION["mail"]=$response[0]["mail"];
    $_SESSION["password"]=$response[0]["password"];
 }

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header("Content-Type: application/json; charset=utf-8");
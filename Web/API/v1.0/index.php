<?php

/**
 * Receive requests to the API, obtaining the resource,
 * method and parameters of these, and processes them
 * @version 1.1
 */


require_once('includes/conexion.php'); // BD conexion


$version = basename(__DIR__); // API version

session_start();

// --------------------------------------------------------------
// --------------------------------------------------------------
// The resources of the petition are obtained


// Retrieve the request method
$method = strtolower($_SERVER['REQUEST_METHOD']);

// Retrieve the requested resource
$uri = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);

// I split the string into chunks from the API version folder
$uri = explode($version."/",$uri)[1]; 
$uri_array = explode("/",$uri);
$recurso = array_shift($uri_array);



// --------------------------------------------------------------
// --------------------------------------------------------------
// The parameters of the petition are collected


// Retrieve query parameters (GET)
$query_params = $_GET;

// Get the GET request parameters and their values
$uri_params = array();
for($i =0; $i < count($uri_array); $i++){
    if($uri_array[$i]!= "") $uri_params[$uri_array[$i]] = $uri_array[++$i]; 
};

// The parameters sent through the body of the request received (PUT or POST) are retrieved
$body_params = (array) json_decode(file_get_contents('php://input'));

// The parameters received are retrieved through a form (POST)
$form_params = $_POST;


// --------------------------------------------------------------
// The resource controller called by the user is included
require('controllers/'.$recurso."_controller.php");



// --------------------------------------------------------------
// --------------------------------------------------------------
// The API response to the request is created

$output = array();

$output['metodo'] = $method;
$output['recurso'] = $recurso;
$output['uri_params'] = $uri_params;



// The response obtained from the request made is added to the output array, in the 'data' field
$output['datos'] = $response;
if(isset($recursoExtra)){
    $output['recursoExtra'] = $recursoExtra;
}

// The output array is encoded as JSON to display it on the screen
echo json_encode($output);
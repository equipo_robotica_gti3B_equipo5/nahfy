<?php
/**
 * Get all of data of the measures from BD
 * @version 1.1
 */

$sql = "SELECT * FROM measures";
$result = mysqli_query($conexion, $sql);

// Store the response in a associative array
$response = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($response, $row);
};
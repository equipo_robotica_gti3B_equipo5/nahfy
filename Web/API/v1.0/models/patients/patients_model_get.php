<?php
/**
 * Get all data from patients of BD
 * @version 1.1
 */

if (isset($query_params["id"])){
    $id = $query_params["id"];
    $sql = "SELECT patients.id, patients.name, patients.surnames, patients.image, patients.telephon, patients.nif, patients.idRoom, rooms.room, rooms.idMap FROM `patients`, `rooms` WHERE patients.idRoom = rooms.id and rooms.idMap = ".$id."";
}else if (isset($query_params["idPatient"])){
    $id = $query_params["idPatient"];
    $sql = "SELECT * FROM patients WHERE patients.id = ".$id."";
}else if (isset($query_params["idCenter"])){
    $id = $query_params["idCenter"];
    $sql = "SELECT patients.id FROM patients, rooms, maps WHERE patients.idRoom = rooms.id and rooms.idMap = maps.id and maps.idCenter = ".$id."";
}else {
    $sql = "SELECT patients.id, patients.name, patients.surnames, patients.image, patients.telephon, patients.nif, patients.idRoom, rooms.room, rooms.idMap FROM `patients`, `rooms` WHERE patients.idRoom = rooms.id";
}

$result = mysqli_query($conexion, $sql);

// Store the response in a associative array
$response = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($response, $row);
};

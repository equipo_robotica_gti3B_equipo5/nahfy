<?php
/**
 * Get all of data of measure type from BD
 * Obtiene todas las datos de tipo de medida de la BBDD
 * @version 1.1
 */

$sql = "SELECT * FROM typemeasure";
$result = mysqli_query($conexion, $sql);

// Store the response in a associative array
$response = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($response, $row);
};
<?php
/**
 * Get all of incidents of BD
 * @version 1.1
 */
if (isset($query_params["id"])){
    $id = $query_params["id"];
    $sql = "SELECT incidents.id, incidents.color, incidents.problem, incidents.isAlert, incidents.idPatient, patients.name, patients.image, patients.surnames, patients.nif, users.email, centers.healthArea, rooms.room, customerscenters.idCenter, customerscenters.idCustomer FROM incidents, users, patients, centers, rooms, maps, customerscenters WHERE incidents.idPatient = patients.id and patients.idRoom = rooms.id and rooms.idMap = maps.id and maps.idCenter = centers.idCenter and users.id = customerscenters.idCustomer  and centers.idCenter= customerscenters.idCenter and incidents.type = 'patient' and incidents.idPatient = ".$id."";
}else{
    $sql = "SELECT incidents.id, incidents.color, incidents.problem, incidents.isAlert, incidents.idPatient, patients.name, patients.image, patients.surnames, patients.nif, users.email, centers.healthArea, rooms.room, customerscenters.idCenter, customerscenters.idCustomer FROM incidents, users, patients, centers, rooms, maps, customerscenters WHERE incidents.idPatient = patients.id and patients.idRoom = rooms.id and rooms.idMap = maps.id and maps.idCenter = centers.idCenter and users.id = customerscenters.idCustomer  and centers.idCenter= customerscenters.idCenter and incidents.type = 'patient'";
}


$result = mysqli_query($conexion, $sql);

// Store the response in a associative array
$response = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($response, $row);
};
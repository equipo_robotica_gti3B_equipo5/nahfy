<?php
/**
 * Get all of incidents of BD
 * @version 1.1
 */

$sql = "SELECT incidents.id, incidents.problem, incidents.description, incidents.idRobot, robots.name, users.email, centers.healthArea, maps.plan, robots.x_now, robots.y_now, customerscenters.idCenter, customerscenters.idCustomer, maps.name as nameMap FROM incidents, users, robots, centers, maps,customerscenters WHERE incidents.type = 'robot' and incidents.idRobot = robots.id and robots.idCenter = centers.idCenter and robots.idMap = maps.id and centers.idCenter= customerscenters.idCenter and users.id = customerscenters.idCustomer";
$result = mysqli_query($conexion, $sql);

// Store the response in a associative array
$response = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($response, $row);
};
<?php
/**
 * Obtain all of the data of clients in BD
 * @version 1.1
 */

$sql = "SELECT users.id FROM users";
$result = mysqli_query($conexion, $sql);

// Store the response in a associative array
$response = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($response, $row);
};
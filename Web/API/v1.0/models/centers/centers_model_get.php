<?php
/**
 * Obtain all of data of the center from the database 
 * @version 1.1
 */

$sql = "SELECT * FROM centers";
$result = mysqli_query($conexion, $sql);

// Store the response in a associative array
$response = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($response, $row);
};